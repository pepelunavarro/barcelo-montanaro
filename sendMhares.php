<?php
/**
 * Created by PhpStorm.
 * User: german
 * Date: 08/01/2019
 * Time: 11:36
 */

if (isset($_GET['sendForm'])) {

    if (isset($_GET['callback'])) {

        $tags = array(
            '{fullName}',
            '{email}',
            '{subject}',
            '{message}'
        );

        $tras = array(
            $_GET['data']['name'],
            $_GET['data']['email'],
            $_GET['data']['subject'],
            $_GET['data']['message'],
        );

        $html = file_get_contents('contactMhares.html');
        $body = str_replace($tags, $tras, $html);

        $email = 'german.dragut@gmail.com';

        $test = sendEmail('no-reply@mharesseaclub.com', utf8_decode('Contacto desde mharesseaclub.com'), utf8_decode('Contacto'), stripslashes($body));

        if ($test === true) {
            $array = array(
                'message' => 'success',
                'text-message' => '',
            );
            echo $_GET['callback'] . '(' . json_encode($array) . ')';
        } else {
            if (isset($_GET['callback'])) {
                $array = array(
                    'message' => 'error',
                    'text-message' => 'Algo salió mal. <br>Por favor inténtelo más tarde.',
                    'error' => $test,
                );
                echo $_GET['callback'] . '(' . json_encode($array) . ')';
            }
        }
    }
}

function sendEmail($from, $from_name, $subject, $body) {

    global $error;
    require_once 'PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer();

    $mail->SMTPDebug = 0;
    $mail->IsSMTP();

    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Host = 'mharesseaclub-com.correoseguro.dinaserver.com';
    $mail->Port = 465;
    $mail->Username = 'no-reply@mharesseaclub.com';
    $mail->Password = 'mhares2019';
    $mail->isHTML(true);
    $mail->SetFrom($from, $from_name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AddAddress('cbn@mharesseaclub.com');
    $mail->AddCC('german.dragut@gmail.com');
    if (!$mail->Send()) {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        return $error;
    } else {
        $error = 'Message sent!';
        return true;
    }
}
