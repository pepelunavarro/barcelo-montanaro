import {Component, OnInit} from '@angular/core';
import {FirestoreService} from '../services/data/firestore.service';
import {ActivatedRoute} from '@angular/router';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import * as $ from 'jquery';
import {SeoService} from '../services/seo/seo.service';

@Component({
    selector: 'app-blog-details',
    templateUrl: './blog-details.page.html',
    styleUrls: ['./blog-details.page.scss'],
})
export class BlogDetailsPage implements OnInit {

    public blog;
    public blogId;
    public timestamp;
    public image;
    public date;
    public url;
    public lang = 'es';
    screenW = window.innerWidth;

    constructor(private route: ActivatedRoute, private firestore: FirestoreService, private socialSharing: SocialSharing, public seo: SeoService) {
        this.blogId = this.route.snapshot.paramMap.get('id');
        this.blog = this.firestore.getBlog(this.blogId).valueChanges();

        const months_arr = {
            'en':
                ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            'es':
                [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre'
                ]
        };

        this.blog.subscribe(res => {
            this.timestamp = res.timestamp;
            this.image = res.img;
            const date = new Date(this.timestamp);
            const year = date.getFullYear();
            const month = months_arr['es'][date.getMonth()];
            const day = date.getDate();
            this.date = day + ' de ' + month + ' de ' + year;

            this.url = 'https://' + window.location.host + '/blog/' + res.id;
        });

    }

    onScroll(e) {
        if (e.detail.deltaY < 0) {
            $('.move-var').css('top', '0');
        } else if(e.detail.deltaY >= 0) {
            $('.move-var').css('top', -100);
        }
    }

    ngOnInit() {
        /*
        this.seo.addTwitterCard(
            '',
            '',
            ''
        );

        this.seo.addFacebookMeta( '',
            '',
            '',
            ''
        );
        */
    }
}
