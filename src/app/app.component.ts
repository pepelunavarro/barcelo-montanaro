import {Component} from '@angular/core';
import {Platform, PopoverController} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Globalization} from '@ionic-native/globalization/ngx';
import * as $ from 'jquery';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';
import {GoogleAnalytics} from '@ionic-native/google-analytics/ngx';
import {LanguageMenuPage} from './language-menu/language-menu.page';
import {FirestoreService} from './services/data/firestore.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    public lang = localStorage.getItem('lang');
    public appPages;
    public currentLang;
    public langSlug;
    public otherLang;
    public otherSlug;
    public thirdLang;
    public thirdSlug;
    public checkLang;
    public routeContact;
    public menu;
    public screenW = window.innerWidth;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private globalization: Globalization,
        private router: Router,
        private ga: GoogleAnalytics,
        // private fbq: Facebook,
        public popoverController: PopoverController,
        private _fs: FirestoreService
    ) {
        if (localStorage.getItem('lang') === null) {
            localStorage.setItem('lang', 'es');
            this.lang = 'es';
        } else {
            this.lang = localStorage.getItem('lang');
        }
        this.appPages = this._fs.getData('itemsMenu', this.lang).valueChanges().subscribe( menu => {
            // @ts-ignore
            this.appPages = menu.data;
        });
        this.router.events.subscribe(event => {
            if (event instanceof  NavigationStart) {
                console.log(event.url);
                if (event.url.search('/es') === 0) {
                    localStorage.setItem('lang', 'es');
                    this.lang = 'es';
                } else if (event.url.search('/en') === 0) {
                    localStorage.setItem('lang', 'en');
                    this.lang = 'en';
                } else if (event.url.search('/de') === 0) {
                    localStorage.setItem('lang', 'de');
                    this.lang = 'de';
                }
            }
            if (event instanceof NavigationEnd) {
                (<any>window).ga('set', 'page', event.urlAfterRedirects);
                (<any>window).ga('send', 'pageview');
                // (<any>window).fbq('track', 'PageView');
                if (event.urlAfterRedirects.search('/upcoming-events/') === 0) {
                    $('.header-desktop').css({
                        'background-color': 'rgba(122,165,186, 0.8)',
                        'background-image': 'none'
                    });
                } else {
                    $('.header-desktop').css({
                        'background-color': 'transparent',
                        'background-image': 'url("/assets/images/background-header-desktop.png")'
                    });
                }
            }
        });

        switch (this.lang) {
            case 'es':
                this.currentLang = 'Español';
                this.langSlug = 'es';
                this.otherLang = 'English';
                this.otherSlug = 'en';
                this.thirdLang = 'DEUTSCH';
                this.thirdSlug = 'de';
                break;
            case 'en':
                this.currentLang = 'English';
                this.langSlug = 'es';
                this.otherLang = 'Español';
                this.otherSlug = 'es';
                this.thirdLang = 'DEUTSCH';
                this.thirdSlug = 'de';
                break;
            case 'de':
                this.currentLang = 'DEUTSCH';
                this.langSlug = 'de';
                this.otherLang = 'English';
                this.otherSlug = 'en';
                this.thirdLang = 'Spanish';
                this.thirdSlug = 'es';
                break;
        }

        this.checkLang = this.globalization.getPreferredLanguage()
            .then(res => console.log('res', res))
            .catch(e => {
                if (e === 'cordova_not_available') {
                    console.log('error', e);
                    return false;
                }
            });
        this.initializeApp();
    }

    languages = {
        es: {
            address: 'Calle Conflent 1B<br>' +
                '07012 Palma de Mallorca<br>' +
                'Telf. <a href="tel:+34971726313">971 72 63 13</a>',
        },
        en: {
            address: 'Calle Conflent 1B<br>' +
                '07012 Palma de Mallorca<br>' +
                'Telf. <a href="tel:+34971726313">971 72 63 13</a>',
        },
        de: {
            address: 'Calle Conflent 1B<br>' +
                '07012 Palma de Mallorca<br>' +
                'Telf. <a href="tel:+34971726313">971 72 63 13</a>',
        },
    };

    goTo(param) {
        window.open(param, '_blank');
    }

    callTo() {
        window.open('tel:+', '_self');
    }

    langSwitch(lang) {
        localStorage.setItem('lang', lang);
        this.lang = lang;
        window.location.href = '/' + lang;
    }

    async presentPopoverMobile(ev: any) {
        const popover = await this.popoverController.create({
            component: LanguageMenuPage,
            event: ev,
            cssClass: 'language-pop-over-mobile',
            translucent: true,
            showBackdrop: true,
        });
        return await popover.present();
    }

    async hideAllMenu() {
        $('.submenu').slideUp();
    }

    async showSubMenu(i) {
        switch (i) {
            case 0:
                $('.menu-2, .menu-4').slideUp();
                break;
            case 2:
                $('.menu-0, .menu-4').slideUp();
                break;
            case 4:
                $('.menu-0, .menu-2').slideUp();
                break;
        }
        $('.menu-' + i).slideDown();
    }

    async hideSubMenu(i) {
        $('.menu-' + i).slideUp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            $('.dropDownMenuLang').on('click', function () {
                $('.dropDownLang').slideToggle();
            });
            $('.dropDownMenu').on('click', function () {
                const id = $(this).attr('data-id');
                $(this).toggleClass('blue');
                $('.menu-' + id).slideToggle();
            });
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
}
