import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';

@Component({
    selector: 'app-custom-header',
    templateUrl: './custom-header.component.html',
    styleUrls: ['./custom-header.component.scss']
})
export class CustomHeaderComponent implements OnInit {

    public lang = localStorage.getItem('lang');
    screenW = window.innerWidth;
    public appPages;
    public currentLang;
    public langSlug;
    public otherLang;
    public otherSlug;
    public thirdLang;
    public thirdSlug;
    public routeContact;

    constructor() {
        switch (this.lang) {
            case 'es':
                this.currentLang = 'Español';
                this.langSlug = 'es';
                this.otherLang = 'Inglés';
                this.otherSlug = 'en';
                this.thirdLang = 'Deutsch';
                this.thirdSlug = 'de';
                this.routeContact = '/contacto';
                this.appPages = [
                    {
                        title: 'SEA CLUB',
                        url: '/es/sea-club',
                        submenu: [{
                            title: 'HAMACAS',
                            url: '/es/hamacas'
                        }]
                    },
                    {
                        title: 'RESTAURANTE',
                        url: '/es/restaurante',
                    },
                    {
                        title: 'MUSICA y CULTURa',
                        url: '/es/musica-y-cultura',
                        submenu: [{
                            title: 'Próximos<br>eventos',
                            url: '/es/upcoming-events'
                        }, {
                            title: 'puesta de sol',
                            url: '/es/puesta-de-sol'
                        }, {
                            title: 'suscríbete',
                            url: '/es/suscribete'
                        }
                        ]
                    },
                    {
                        title: 'Galería',
                        url: '/es/galeria-de-fotos'
                    },
                    {
                        title: 'Sobre nosotros',
                        url: '/es/sobre-nosotros',
                        submenu: [
                            {
                                title: 'Filosofía',
                                url: '/es/filosofia'
                            },
                            {
                                title: 'Trabaja<br>con nosotros',
                                url: '/es/trabaja-con-nosotros'
                            },
                            {
                                title: 'contacto',
                                url: '/es/contacto'
                            }
                        ]
                    },
                    {
                        title: 'Miembros',
                        url: '/es/miembros',
                    },
                    {
                        title: 'Reservar',
                        url: 'https://mharesseaclub.myrestoo.net/es/reservar'
                    }
                ];
                break;
            case 'en':
                this.currentLang = 'English';
                this.langSlug = 'es';
                this.otherLang = 'Spanish';
                this.otherSlug = 'es';
                this.thirdLang = 'Deutsch';
                this.thirdSlug = 'de';
                this.routeContact = '/en/contact';
                this.appPages = [
                    {
                        title: 'SEA CLUB',
                        url: '/en/sea-club',
                        submenu: [{
                            title: 'SUNBEDS',
                            url: '/en/sunbeds'
                        }]
                    },
                    {
                        title: 'RESTAURANT',
                        url: '/en/restaurant',
                    },
                    {
                        title: 'MUSIC & CULTURE',
                        url: '/en/music-and-culture',
                        submenu: [{
                            title: 'Upcoming<br>events',
                            url: '/upcoming-events'
                        }, {
                            title: 'sunsets',
                            url: '/en/sunset'
                        }, {
                            title: 'subscribe',
                            url: '/en/subscribe'
                        }
                        ]
                    },
                    {
                        title: 'Gallery',
                        url: '/en/gallery'
                    },
                    {
                        title: 'About us',
                        url: '/en/about-us',
                        submenu: [
                            {
                                title: 'philosophy',
                                url: '/en/philosophy'
                            },
                            {
                                title: 'work<br>with us',
                                url: '/en/work-with-us'
                            },
                            {
                                title: 'contact',
                                url: '/en/contact'
                            }
                        ]
                    },
                    {
                        title: 'MEMBERS',
                        url: '/en/members',
                    },
                    {
                        title: 'Book Now',
                        url: 'https://mharesseaclub.myrestoo.net/es/reservar',
                    },
                ];
                break;
            case 'de':
                this.currentLang = 'DEUTSCH';
                this.langSlug = 'de';
                this.otherLang = 'English';
                this.otherSlug = 'en';
                this.thirdLang = 'Spanish';
                this.thirdSlug = 'es';
                this.routeContact = '/contact';
                this.appPages = [
                    {
                        title: 'SEA CLUB',
                        url: '/sea-club',
                        submenu: [{
                            title: 'SONNENLIEGEN',
                            url: '/sunbeds'
                        }]
                    },
                    {
                        title: 'RESTAURANT',
                        url: '/restaurant',
                    },
                    {
                        title: 'MUSIK UND KULTURVERANSTALTUNGEN',
                        url: '/music-and-culture',
                        submenu: [{
                            title: 'Musik und Kultur',
                            url: '/upcoming-events'
                        }, {
                            title: 'SONNENUNTERGÄNGE',
                            url: '/sunset'
                        }, {
                            title: 'ABONNEMENT',
                            url: '/subscribe'
                        }
                        ]
                    },
                    {
                        title: 'GALERIE',
                        url: '/gallery'
                    },
                    {
                        title: 'UBER UNS',
                        url: '/about-us',
                        submenu: [
                            {
                                title: 'PHILOSOPHIE',
                                url: '/philosophy'
                            },
                            {
                                title: 'STELLENANGEBOTE',
                                url: '/work-with-us'
                            },
                            {
                                title: 'KONTAKT',
                                url: '/contact'
                            }
                        ]
                    },
                    {
                        title: 'MITGLIEDER',
                        url: '/members',
                    },
                ];
                break;
        }
    }

    languages = {
        es: {
            brandUrl: '/es',
            bookNow: 'RESERVAR',
            bookNowUrl: 'https://mharesseaclub.myrestoo.net/es/reservar',
            schedulesTitle: 'SCHEDULES',
            beachClub: 'Beach Club 11:00h - 20:30h',
            swimmingPool: 'Swimming Pool 11:00h - 19:00h',
            restaurant: 'Restaurant 13:00h - 16:30h | 19:00h - 22:00h',
        },
        en: {
            brandUrl: '/en',
            bookNow: 'BOOK NOW',
            bookNowUrl: 'https://mharesseaclub.myrestoo.net/en/reservar',
            schedulesTitle: 'SCHEDULES',
            beachClub: 'Beach Club 11:00h - 20:30h',
            swimmingPool: 'Swimming Pool 11:00h - 19:00h',
            restaurant: 'Restaurant 13:00h - 16:30h | 19:00h - 22:00h',
        },
        de: {
            brandUrl: '/de',
            bookNow: 'JETZT BUCHEN',
            bookNowUrl: 'https://mharesseaclub.myrestoo.net/de/reservar',
            schedulesTitle: 'Zeitpläne',
            beachClub: 'Beach Club 11:00h - 20:30h',
            swimmingPool: 'Schwimmbad 11:00h - 19:00h',
            restaurant: 'Restaurant 13:00h - 16:30h | 19:00h - 22:00h',
        },
    };

    async hideAllMenu() {
        $('.submenu').slideUp();
    }

    async showSubMenu(i) {
        switch (i) {
            case 0:
                $('.menu-2, .menu-4').slideUp();
                break;
            case 2:
                $('.menu-0, .menu-4').slideUp();
                break;
            case 4:
                $('.menu-0, .menu-2').slideUp();
                break;
        }
        $('.menu-' + i).slideDown();
    }

    async hideSubMenu(i) {
        $('.menu-' + i).slideUp();
    }


    ngOnInit() {}
}
