import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-custom-footer',
    templateUrl: './custom-footer.component.html',
    styleUrls: ['./custom-footer.component.scss']
})
export class CustomFooterComponent implements OnInit {

    public lang = localStorage.getItem('lang');
    public languages = {
        es: {
            bookNow: 'RESERVAR',
            bookNowLink: 'https://mharesseaclub.myrestoo.net/es/reservar'
        },
        en: {
            bookNow: 'BOOK NOW',
            bookNowLink: 'https://mharesseaclub.myrestoo.net/en/reservar'
        },
        de: {
            bookNow: 'JETZT BUCHEN',
            bookNowLink: 'https://mharesseaclub.myrestoo.net/de/reservar'
        }
    };

    constructor() {}

    goTo(url) {
        window.open(url, '_blank');
    }

    ngOnInit() {}

}
