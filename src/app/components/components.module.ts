import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomHeaderComponent} from './custom-header/custom-header.component';
import {IonicModule} from '@ionic/angular';
import {CustomFooterComponent} from './custom-footer/custom-footer.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AgmCoreModule} from '@agm/core';
import {AgmSnazzyInfoWindowModule} from '@agm/snazzy-info-window';
import {AgmJsMarkerClustererModule} from '@agm/js-marker-clusterer';
import {RouterModule} from '@angular/router';
import {CookiesPolicyModalComponent} from './cookies-policy-modal/cookies-policy-modal.component';
import { CustomTitlebarComponent } from './custom-titlebar/custom-titlebar.component';
import { CustomThumbnailComponent } from './custom-thumbnail/custom-thumbnail.component';
import { CustomToolbarComponent } from './custom-toolbar/custom-toolbar.component';
import { ImageFadeComponent } from './image-fade/image-fade.component';

@NgModule({
    declarations: [
        CustomHeaderComponent,
        CustomFooterComponent,
        CookiesPolicyModalComponent,
        CustomTitlebarComponent,
        CustomThumbnailComponent,
        CustomToolbarComponent,
        ImageFadeComponent        
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        AgmCoreModule,
        AgmSnazzyInfoWindowModule,
        AgmJsMarkerClustererModule,
        RouterModule
    ],
    exports: [
        CustomHeaderComponent,
        CustomTitlebarComponent,
        CustomFooterComponent,
        RouterModule,
        CookiesPolicyModalComponent,
        CustomThumbnailComponent,
        CustomToolbarComponent,
        ImageFadeComponent
    ]
})
export class ComponentsModule {
}
