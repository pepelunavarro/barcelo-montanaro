import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageFadeComponent } from './image-fade.component';

describe('ImageFadeComponent', () => {
  let component: ImageFadeComponent;
  let fixture: ComponentFixture<ImageFadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageFadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageFadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
