import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-fade',
  templateUrl: './image-fade.component.html',
  styleUrls: ['./image-fade.component.scss']
})
export class ImageFadeComponent implements OnInit {

  @Input()
  imagen: String;


  constructor() { }

  ngOnInit() {
  }

}
