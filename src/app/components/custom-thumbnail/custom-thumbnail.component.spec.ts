import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomThumbnailComponent } from './custom-thumbnail.component';

describe('CustomThumbnailComponent', () => {
  let component: CustomThumbnailComponent;
  let fixture: ComponentFixture<CustomThumbnailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomThumbnailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomThumbnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
