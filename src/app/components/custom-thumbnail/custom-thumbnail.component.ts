import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custom-thumbnail',
  templateUrl: './custom-thumbnail.component.html',
  styleUrls: ['./custom-thumbnail.component.scss']
})
export class CustomThumbnailComponent implements OnInit {

  @Input()
  titulo: String;
  @Input()
  imagen: String;
  @Input()
  link: String;

  constructor() { }

  ngOnInit() {
  }

}
