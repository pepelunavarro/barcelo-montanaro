import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-cookies-policy-modal',
  templateUrl: './cookies-policy-modal.component.html',
  styleUrls: ['./cookies-policy-modal.component.scss']
})
export class CookiesPolicyModalComponent implements AfterViewInit, OnInit {

  cookie = 'false';
  active = '';
  show = '';
  lang = localStorage.getItem('lang');

  constructor() {
      this.cookie = localStorage.getItem('cookies-policy');
  }

    languages = {
        es : {
            title: 'Aviso de cookies',
            ok: 'Aceptar',
            more: 'Más info',
            url: '/politica-de-cookies'
        },
        en : {
            title: 'Cookies policy',
            ok: 'Ok',
            more: 'More info',
            url: '/cookies-law'
        },
        de : {
            title: 'Cookies policy',
            ok: 'Ok',
            more: 'More info',
            url: '/cookies-law'
        }
    };
  ngAfterViewInit() {
      if (this.cookie === null) {
          setTimeout(() => {
              this.show = 'show';
          }, 0);
          setTimeout(() => {
              this.active = 'active';
          }, 1500);
      } else if (this.cookie === 'true') {
          setTimeout(() => {
              this.show = '';
              this.active = '';
          }, 0);
          setTimeout(() => {
              $('.brand-action').addClass('show');
              }, 1000);
      }
  }

  ngOnInit() {}

  putCookie() {
      localStorage.setItem('cookies-policy', 'true');
      $('.cookies-module').removeClass('show active');
      $('.brand-action').addClass('show');
  }
}
