import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {CookiesPolicyModalComponent} from './cookies-policy-modal.component';

describe('CookiesPolicyModalComponent', () => {
  let component: CookiesPolicyModalComponent;
  let fixture: ComponentFixture<CookiesPolicyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookiesPolicyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookiesPolicyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
