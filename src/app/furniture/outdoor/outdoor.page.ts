import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-outdoor',
  templateUrl: './outdoor.page.html',
  styleUrls: ['./outdoor.page.scss'],
})
export class OutdoorPage implements OnInit {

  public lang = localStorage.getItem('lang');
  public languages = {
        es: {
            title: 'Mobiliario de exterior',
        },
        en: {
            title: 'Mobiliario de exterior',
        },
        de: {
            title: 'Mobiliario de exterior',
        }
  }
  public items = [{es:'Dormitorios',en:'Dormitorios',de:'Dormitorios',img:'decoration/image-one.jpg'},
  				{es:'Salón',en:'Salón',de:'Salón',img:'decoration/image-one.jpg'},
  				{es:'Comedor',en:'Comedor',de:'Comedor',img:'decoration/image-one.jpg'},
  				{es:'Iluminación',en:'Iluminación',de:'Iluminación',img:'decoration/image-one.jpg'},
  				{es:'Accesorios',en:'Accesorios',de:'Accesorios',img:'decoration/image-one.jpg'},
  			   	 ];

  constructor() { }

  ngOnInit() {
  }

}
