import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-indoor',
  templateUrl: './indoor.page.html',
  styleUrls: ['./indoor.page.scss'],
})
export class IndoorPage implements OnInit {

  public ruta;
  public lang = localStorage.getItem('lang');
  public languages = {
        es: {
            title: 'Mobiliario de interior',
        },
        en: {
            title: 'Mobiliario de interior',
        },
        de: {
            title: 'Mobiliario de interior',
        }
  }
  public items = [{es:'Dormitorios',en:'Dormitorios',de:'Dormitorios',img:'decoration/image-one.jpg'},
  				{es:'Salón',en:'Salón',de:'Salón',img:'decoration/image-one.jpg'},
  				{es:'Comedor',en:'Comedor',de:'Comedor',img:'decoration/image-one.jpg'},
  				{es:'Iluminación',en:'Iluminación',de:'Iluminación',img:'decoration/image-one.jpg'},
  				{es:'Accesorios',en:'Accesorios',de:'Accesorios',img:'decoration/image-one.jpg'},
  			   	 ];

  constructor( public router: Router) { this.ruta=this.router.url; }

  ngOnInit() {
  }

}
