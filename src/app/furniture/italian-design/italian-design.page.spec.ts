import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItalianDesignPage } from './italian-design.page';

describe('ItalianDesignPage', () => {
  let component: ItalianDesignPage;
  let fixture: ComponentFixture<ItalianDesignPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItalianDesignPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItalianDesignPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
