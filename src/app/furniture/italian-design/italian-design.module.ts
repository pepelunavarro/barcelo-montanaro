import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ItalianDesignPage } from './italian-design.page';

const routes: Routes = [
  {
    path: '',
    component: ItalianDesignPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ItalianDesignPage]
})
export class ItalianDesignPageModule {}
