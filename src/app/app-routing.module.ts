import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [

    {path: '', redirectTo: 'es', pathMatch: 'full'},
    {path: 'es', loadChildren: './home/home.module#HomePageModule'},
    {path: 'en', loadChildren: './home/home.module#HomePageModule'},

    {path: 'es/noticias', loadChildren: './blog-list/blog-list.module#BlogListPageModule'},
    {path: 'es/noticias/:id', loadChildren: './blog-details/blog-details.module#BlogDetailsPageModule'},

    {path: 'en/news', loadChildren: './blog-list/blog-list.module#BlogListPageModule'},
    {path: 'en/news/:id', loadChildren: './blog-details/blog-details.module#BlogDetailsPageModule'},

    {path: 'es/contacto', loadChildren: './contact/contact.module#ContactPageModule'},
    {path: 'en/contact', loadChildren: './contact/contact.module#ContactPageModule'},

    {path: 'es/sobre-nosotros', loadChildren: './about-us/about-us.module#AboutUsPageModule'},
    {path: 'en/about-us', loadChildren: './about-us/about-us.module#AboutUsPageModule'},

    {path: 'en/indoor', loadChildren: './furniture/indoor/indoor.module#IndoorPageModule'},
    {path: 'es/interior', loadChildren: './furniture/indoor/indoor.module#IndoorPageModule'},

    {path: 'en/indoor/:id', loadChildren: './furniture/accessories/accessories.module#AccessoriesPageModule'},    
    {path: 'es/interior/:id', loadChildren: './furniture/accessories/accessories.module#AccessoriesPageModule'},

    {path: 'en/outdoor', loadChildren: './furniture/outdoor/outdoor.module#OutdoorPageModule'},
    {path: 'es/exterior', loadChildren: './furniture/outdoor/outdoor.module#OutdoorPageModule'},

    {path: 'en/outdoor/:id', loadChildren: './furniture/accessories/accessories.module#AccessoriesPageModule'},    
    {path: 'es/exterior/:id', loadChildren: './furniture/accessories/accessories.module#AccessoriesPageModule'},

    {path: 'en/italian-design', loadChildren: './furniture/italian-design/italian-design.module#ItalianDesignPageModule'},
    {path: 'es/italiano', loadChildren: './furniture/italian-design/italian-design.module#ItalianDesignPageModule'},

    {path: 'en/companies', loadChildren: './companies/companies.module#CompaniesPageModule'},
    {path: 'es/firmas', loadChildren: './companies/companies.module#CompaniesPageModule'},

    {path: 'en/decor', loadChildren: './bm-services/decor/decor.module#DecorPageModule'},
    {path: 'es/decoracion', loadChildren: './bm-services/decor/decor.module#DecorPageModule'},

    {path: 'en/integral-interiorism', loadChildren: './bm-services/interior-design/interior-design.module#InteriorDesignPageModule'},
    {path: 'es/interiorismo-integral', loadChildren: './bm-services/interior-design/interior-design.module#InteriorDesignPageModule'},

    {path: 'en/interior-design', loadChildren: './bm-services/interior-design/interior-design.module#InteriorDesignPageModule'},
    {path: 'es/disenio-interior', loadChildren: './bm-services/interior-design/interior-design.module#InteriorDesignPageModule'},

    {path: 'en/reforms', loadChildren: './bm-services/reforms/reforms.module#ReformsPageModule'},
    {path: 'es/reformas', loadChildren: './bm-services/reforms/reforms.module#ReformsPageModule'},

    {path: 'en/portfolio', loadChildren: './trends/portfolio/portfolio.module#PortfolioPageModule'},
    {path: 'es/exposition', loadChildren: './trends/exposition/exposition.module#ExpositionPageModule'},

    {path: 'en/news', loadChildren: './trends/news/news.module#NewsPageModule'},
    {path: 'es/es-noticia', loadChildren: './trends/news/news.module#NewsPageModule'},

    {path: 'en/collections', loadChildren: './trends/collections/collections.module#CollectionsPageModule'},
    {path: 'es/colecciones', loadChildren: './trends/collections/collections.module#CollectionsPageModule'},

    {path: 'en/contact', loadChildren: './about/contact/contact.module#ContactPageModule'},
    {path: 'es/contacto', loadChildren: './about/contact/contact.module#ContactPageModule'},

    {path: 'en/our-stores', loadChildren: './about/our-stores/our-stores.module#OurStoresPageModule'},
    {path: 'es/nuestras-tiendas', loadChildren: './about/our-stores/our-stores.module#OurStoresPageModule'},

    {path: 'en/about-us', loadChildren: './about/about-us/about-us.module#AboutUsPageModule'},
    {path: 'en/acerca-de-nosotros', loadChildren: './about/about-us/about-us.module#AboutUsPageModule'},

    {
        path: 'legal-note-and-terms-of-use',
        loadChildren: './legal-note-and-terms-of-use/legal-note-and-terms-of-use.module#LegalNoteAndTermsOfUsePageModule'
    },
    {
        path: 'nota-legal-y-condiciones-de-uso',
        loadChildren: './legal-note-and-terms-of-use/legal-note-and-terms-of-use.module#LegalNoteAndTermsOfUsePageModule'
    },
    {path: '404', loadChildren: './not-found/not-found.module#NotFoundPageModule'},
    {path: '**', redirectTo: '/404'},

    {path: 'politica-de-privacidad', loadChildren: './privacy-policy/privacy-policy.module#PrivacyPolicyPageModule'},
    {path: 'privacy-policy', loadChildren: './privacy-policy/privacy-policy.module#PrivacyPolicyPageModule'},
    {path: 'datenschutzerklarung', loadChildren: './privacy-policy/privacy-policy.module#PrivacyPolicyPageModule'},
    {path: 'politica-de-cookies', loadChildren: './cookies-policy/cookies-policy.module#CookiesPolicyPageModule'},
    {path: 'cookies-policy', loadChildren: './cookies-policy/cookies-policy.module#CookiesPolicyPageModule'},
    {path: 'marketing-policy', loadChildren: './marketing-policy/marketing-policy.module#MarketingPolicyPageModule'},
    {path: 'politica-de-marketing', loadChildren: './marketing-policy/marketing-policy.module#MarketingPolicyPageModule'},
  
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
