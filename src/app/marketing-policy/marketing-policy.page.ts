import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-marketing-policy',
  templateUrl: './marketing-policy.page.html',
  styleUrls: ['./marketing-policy.page.scss'],
})
export class MarketingPolicyPage implements OnInit {
    public lang = 'es';
    screenW = window.innerWidth;

    constructor() {
    }

    languages = {
        es: {
            section: 'Política de Marketing',
            title: 'Última actualización: junio 2019 ',
            text: '<p>Esta política de privacidad se aplica a las comunicaciones ' +
                'comerciales que recibe de COQUIMBAS S.L.</p>' +
                '<p>Por favor, léala detenidamente. En ella encontrará información ' +
                'importante sobre el tratamiento de sus datos personales y los derechos' +
                ' que le reconoce la normativa vigente en la materia.</p>' +
                '<p>Nos reservamos el derecho de actualizar nuestra política de ' +
                'privacidad en cualquier momento con motivo de decisiones empresariales, ' +
                'así como para cumplir con eventuales cambios legislativos o jurisprudenciales. ' +
                'Si tiene dudas o necesita cualquier aclaración respecto a nuestra Política de ' +
                'privacidad o a sus derechos, puede contactar con nosotros a través de los ' +
                'canales que se indican más abajo</p>',
            arrayText: [
                {
                    title: '1. ¿Quién es el responsable del tratamiento de sus datos?',
                    text: '<p>El responsable del tratamiento de sus datos es COQUIMBAS S.L., con N.I.F. ' +
                        'B-57699498  domicilio en Avda. Jaime III 4-2o' +
                        ', 07012 Palma de Mallorca, Illes Balears, España y correo ' +
                        'electrónico <a href="mailto:_SOCIAL_EMAIL">_SOCIAL_EMAIL</a></p>'
                },
                {
                    title: '2. ¿Qué información personal obtenemos?',
                    text: '<p>Los datos utilizados para el envío de comunicaciones comerciales son los que se obtienen:</p>' +
                        '<ul>' +
                        '<li><p>De los formularios que Ud. cumplimente y de las solicitudes que formule</p></li>' +
                        '</ul>' +
                        '<p>Las categorías de datos que tratamos consisten típicamente en:</p>' +
                        '<ul>' +
                        '<li><p>Datos identificativos y de contacto</p></li>' +
                        '<li><p>Datos de seguimiento comercial</p></li>' +
                        '</ul>'
                },
                {
                    title: '3. ¿Con qué fines tratamos estos datos?',
                    text: '<p>Los datos que obtenemos los utilizamos para remitirle comunicaciones ' +
                        'comerciales  a los correos electrónicos facilitados y para mejora de la calidad ' +
                        'de nuestros productos y servicios.</p>'
                },
                {
                    title: '4. ¿A quién podemos comunicar sus datos?',
                    text: '<p>Sólo comunicaremos sus datos por obligación legal o con su previo consentimiento.</p>'
                },
                {
                    title: '5. Base jurídica de los tratamientos',
                    text: '<p>El envío de comunicaciones comerciales no personalizadas, por ejemplo, boletines se basa en ' +
                        'nuestro interés legítimo en la promoción de nuestros servicios y novedades legales.</p>'
                },
                {
                    title: '6. ¿Cuánto tiempo conservaremos sus datos?',
                    text: '<p>Los datos tratados para fines comerciales se conservan activos mientras tanto el ' +
                        'interesado no revoque su consentimiento o solicite su supresión y, en todo caso, durante ' +
                        'los plazos previstos en las disposiciones legales aplicables y durante el tiempo necesario ' +
                        'para atender a posibles responsabilidades nacidas del tratamiento. Los soportes en los que ' +
                        'constan la existencia de su consentimiento para el tratamiento de sus datos para estos fines, ' +
                        'tales como formularios firmados, logs de envío de formularios electrónicos, serán conservados ' +
                        'durante toda la duración de los tratamientos y los plazos de prescripción aplicables.</p>'
                },
                {
                    title: '7. ¿Cuáles son sus derechos?',
                    text: '<p>Tiene derecho a obtener confirmación de si estamos tratando o no sus datos personales y, ' +
                        'en tal caso, acceder a los mismos.  Puede igualmente pedir que sus datos sean rectificados cuando' +
                        ' sean inexactos o a que se completen los datos que sean incompletos, así como solicitar su supresión ' +
                        'cuando, entre otros motivos, los datos ya no sean necesarios para los fines para los que fueron recogidos.</p>' +
                        '<p>En determinadas circunstancias, podrá solicitar la limitación del tratamiento de sus datos. En tal caso, ' +
                        'sólo trataremos los datos afectados para la formulación, el ejercicio o la defensa de reclamaciones o con ' +
                        'miras a la protección de los derechos de otras personas. </p>' +
                        '<p>En determinadas condiciones y por motivos relacionados con su situación particular, podrá igualmente ' +
                        'oponerse al tratamiento de sus datos. En este caso dejaremos de tratar los datos, salvo por motivos legítimos ' +
                        'imperiosos que prevalezcan sobre sus intereses, derechos y libertades, o para la formulación, el ejercicio o la ' +
                        'defensa de reclamaciones.</p>' +
                        '<p>No obstante, en cualquier momento podrá revocar su consentimiento y oponerse al tratamiento de sus datos con ' +
                        'fines de mercadotecnia directa, incluida la elaboración de perfiles comerciales. ' +
                        'En tal caso dejaremos de tratar su ' +
                        'información personal para dichos fines. La retirada de su consentimiento no afectará a la ' +
                        'licitud del tratamiento basado en el consentimiento previo a la misma. </p>' +
                        '<p>Así mismo y bajo ciertas condiciones podrá solicitar la portabilidad de sus datos para que sean transmitidos ' +
                        'a otro responsable del tratamiento.</p>' +
                        '<p>Tiene igualmente derecho a presentar una reclamación ante la Agencia Española de Protección de Datos o ' +
                        'cualquier otra autoridad de control competente. </p>' +
                        '<p>Para ejercer sus derechos deberá remitirnos una solicitud acompañada de una copia de su documento nacional ' +
                        'de identidad u otro documento válido que le identifique por correo postal o ' +
                        'electrónico a las direcciones indicadas en el apartado ¿Quién es el responsable del ' +
                        'tratamiento de sus datos?.</p>' +
                        '<p>Para revocar su consentimiento al envío de nuestras comunicaciones comerciales bastará con enviar un correo ' +
                        'electrónico a: <a target="_blank" href="mailto:_SOCIAL_EMAIL">_SOCIAL_EMAIL</a> o ' +
                        'utilizar el enlace previsto a tal efecto en nuestros boletines.</p>' +
                        '<p>Podrá obtener más información sobre sus derechos y cómo ejercerlos en la página de la Agencia Española ' +
                        'de Protección de Datos en <a target="_blank" href="mailto:chttp://www.aepd.es">http://www.aepd.es</a></p>'
                },
            ]
        }
    };

    onScroll(e) {
        if (e.detail.deltaY < 0) {
            $('.move-var').css('top', '0');
        } else if (e.detail.deltaY >= 0) {
            $('.move-var').css('top', -100);
        }
    }
  ngOnInit() {
  }

}
