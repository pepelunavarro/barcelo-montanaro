import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketingPolicyPage } from './marketing-policy.page';

describe('MarketingPolicyPage', () => {
  let component: MarketingPolicyPage;
  let fixture: ComponentFixture<MarketingPolicyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketingPolicyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketingPolicyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
