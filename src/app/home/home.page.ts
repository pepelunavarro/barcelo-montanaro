import {Component, ViewChild, OnInit, AfterViewInit} from '@angular/core';
import {Content, LoadingController} from '@ionic/angular';
import {FirestoreService} from '../services/data/firestore.service';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import {SeoService} from '../services/seo/seo.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements AfterViewInit, OnInit {

    @ViewChild(Content)
    content: Content;
    screenW = window.innerWidth;
    public slider;
    lang;
    commonLanguages;
    section;
    events;
    constructor(
        public firestore: FirestoreService,
        public router: Router,
        private seo: SeoService,
    ) {

        this.lang = localStorage.getItem('lang');
        this.commonLanguages = this.firestore.getData('commonLanguages', this.lang).valueChanges();
    }


    onScroll(e) {
        if (e.detail.scrollTop > 50) {
            $('.move-var').css('top', '0');
            $('.header-desktop.fixed-var').addClass('scrolled');
            $('.scroll-down-mouse').fadeOut();
        } else if (e.detail.scrollTop <= 50) {
            $('.move-var').css('top', - 100);
            $('.header-desktop.fixed-var').removeClass('scrolled');
            $('.scroll-down-mouse').fadeIn();
        }
    }

    ngAfterViewInit() {}

    ngOnInit() {
        /*

                this.seo.addTwitterCard(
                    '',
                    '',
                    ''
                );

                this.seo.addFacebookMeta( '',
                    '',
                    '',
                    ''
                );
        */
    }
}
