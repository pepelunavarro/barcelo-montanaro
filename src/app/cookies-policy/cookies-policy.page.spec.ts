import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookiesPolicyPage } from './cookies-policy.page';

describe('CookiesPolicyPage', () => {
  let component: CookiesPolicyPage;
  let fixture: ComponentFixture<CookiesPolicyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookiesPolicyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookiesPolicyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
