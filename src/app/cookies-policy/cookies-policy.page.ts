import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';

@Component({
    selector: 'app-cookies-policy-modal',
    templateUrl: './cookies-policy.page.html',
    styleUrls: ['./cookies-policy.page.scss'],
})
export class CookiesPolicyPage implements OnInit {

    public lang = 'es';
    screenW = window.innerWidth;

    constructor() {
    }

    languages = {
        es: {
            section: 'Política de Cookies',
            title: 'En esta Web utilizamos cookies.',
            subtitle: '¿Para qué utilizamos cookies y tratamos datos de navegación?',
            titleHighlight: 'El Área de Práctica de Derecho Fiscal acumula una amplia ' +
                'experiencia en el tratamiento de la problemática tributaria de nuestros ' +
                'clientes, ya sean personas físicas o jurídicas.',
            whatIsTitle: '¿Qué es una cookie?',
            whatIsDescription: 'Una cookie es un fichero que las webs transfieren a ' +
                'los equipos que se conectan a ellas. Si bien a menudo los propósitos ' +
                'de las cookies son sólo técnicos, también pueden permitir, entre otras ' +
                'cosas, almacenar y recuperar información sobre los hábitos de navegación ' +
                'de un usuario o de su equipo y, dependiendo de la información que contengan ' +
                'y de la forma en que utilice su equipo, utilizarse para reconocer al usuario.<br>' +
                'Existen cookies que son controladas y gestionadas por el propio titular de la Web ' +
                '(son llamadas “cookies propias”) y otras que lo son por terceros (son conocidas como ' +
                '“cookies de terceros”), por ejemplo, porque estos proporcionan una herramienta o una ' +
                'funcionalidad integrada a la web.<br>' +
                'Ciertas cookies se cancelan una vez finalizada la navegación en la web ' +
                '(cookies de sesión) mientras que otras pueden seguir almacenadas en el equipo ' +
                'de los usuarios y ser accedidas por un periodo más largo (cookies persistentes).',
            whatTypeTitle: '¿Qué tipo de cookies utilizamos y para qué?',
            whatTypeSubtitle: 'En está Web utilizamos el siguiente tipo de cookies:',
            whatTypeOneTitle: '1. Cookies técnicas o indispensables a la prestación de los servicios',
            whatTypeOneDescription: 'Utilizamos cookies propias, de carácter técnico, para poder ' +
                'prestar los servicios solicitados por nuestros usuarios. Estas cookies son indispensables ' +
                'para la navegación y la utilización de las diferentes opciones y servicios de la web, como, ' +
                'por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a ' +
                'partes de acceso restringido, utilizar elementos de seguridad durante la navegación o compartir ' +
                'contenidos a través de redes sociales.<br>' +
                '<ul>' +
                '<li><p>Utilizamos las cookies LocalStorage para almacenamiento de información de aplicación, servicios offline, etc.</p></li>' +
                '<li><p>Utilizamos las cookies SessionStorage para el inicio de sesión de usuarios.</p></li>' +
                '</ul>',
            whatTypeTwoTitle: '2. Cookies de personalización',
            whatTypeTwoDescription: 'Con el fin de optimizar la experiencia de uso de nuestra web, ' +
                'utilizamos cookies de personalización que nos permiten configurar la web y sus servicios ' +
                'acorde a las preferencias del usuario o a características generales de su equipo, por ejemplo, ' +
                'el navegador utilizado y su versión, el sistema operativo instalado, etc.<br>' +
                'Estas cookies se utilizan para, por ejemplo, recordar los parámetros que fueron elegidos por el ' +
                'usuario durante sus visitas, orientar el contenido de la web en función de su idioma o ubicación o ' +
                'mantener el estado acreditado de los usuarios registrados.',
            whatTypeTreeTitle: '3. Cookies de análisis',
            whatTypeTreeDescription: 'Las cookies de análisis permiten medir y analizar la actividad de los sitios ' +
                'web y elaborar perfiles de navegación de los usuarios.<br>' +
                'Utilizamos las cookies de GOOGLE ANALYTICS para recopilar estadísticas anónimas y conjuntas que nos ' +
                'permitan comprender la forma en que los usuarios utilizan nuestra web, con el fin de mejorarla y ' +
                'ajustarla a las preferencias de nuestros visitantes.<br>' +
                'GOOGLE ANALYTICS es una herramienta provista por la empresa Google, Inc., 1600 Amphitheatre Parkway. ' +
                'Mountain View, CA 94043. EE.UU. Puede obtener más información sobre el funcionamiento de GOOGLE ANALYTICS ' +
                'y las cookies utilizadas por este servicio en los siguientes enlaces: <br>' +
                '-  <a href="https://support.google.com/analytics/answer/6004245" target="_blank">' +
                'https://support.google.com/analytics/answer/6004245</a><br>' +
                '-  <a href="http://www.google.com/intl/es/policies/privacy/" target="_blank">' +
                'http://www.google.com/intl/es/policies/privacy/</a><br>' +
                'Puede desactivar las cookies de GOOGLE ANALYTICS mediante la instalación en su navegador del complemento ' +
                'de inhabilitación creado por Google y disponible en el siguiente enlace: ' +
                '<a href="https://tools.google.com/dlpage/gaoptout?hl=es" target="_blank">' +
                'https://tools.google.com/dlpage/gaoptout?hl=es</a><br>' +
                'Con la misma finalidad de análisis, nuestra página web utiliza también pixeles de seguimiento o códigos que ' +
                'una vez activados pueden provocar la creación de cookies, o su lectura o modificación en la caché de su navegador. ' +
                'Estos pixeles y cookies se usan para crear o modificar identificadores únicos y rastrear las acciones que el usuario, ' +
                'visualiza y hace click en nuestros anuncios en redes sociales, realiza en nuestras páginas web, para fines de marketing ' +
                'y análisis. En concreto utilizamos un pixel de FACEBOOK, que instalado en nuestra web nos permite conocer estadísticas ' +
                'sobre la efectividad de nuestros anuncios en esta red social. Mas información sobre el uso de esta tecnología para ' +
                'estadísticas y mediciones en la política de cookies de FACEBOOK: ' +
                '<a href="https://www.facebook.com/policies/cookies/" target="_blank">https://www.facebook.com/policies/cookies/</a><br>' +
                'Típicamente los datos derivados de su navegación que pueden ser objeto de análisis son los siguientes:<br>' +
                '<ul>' +
                '<li><p>El nombre de dominio del proveedor (PSI) y/o dirección IP que les da acceso a la red. Por ejemplo, un usuario del ' +
                'proveedor xxx sólo estará identificado con el dominio xxx.es y/o la dirección IP. De esta manera podemos elaborar ' +
                'estadísticas sobre los países y servidores que visitan más a menudo nuestro web.</p></li>' +
                '<li><p>La fecha y hora de acceso a nuestro web. Ello nos permite averiguar las horas de más ' +
                'afluencia, y hacer los ajustes precisos para evitar problemas de saturación en nuestras horas punta.</p></li>' +
                '<li><p>La dirección de internet desde la que partió el link que dirige a nuestro web. Gracias a este dato, ' +
                'podemos conocer la efectividad de los distintos banners y enlaces que apuntan a nuestro servidor, con el ' +
                'fin de potenciar los que ofrezcan mejores resultados.</p></li>' +
                '<li><p>El número de visitantes diarios de cada sección. Ello nos permite conocer las áreas de más éxito y ' +
                'aumentar y mejorar su contenido, con el fin de que los usuarios obtengan un resultado más satisfactorio.</p></li>' +
                '</ul>',
            anonymousTitle: '¿Son anónimas las cookies?',
            anonymousDescription: '<p>Las cookies no contienen información que permita por sí sola identificar a un usuario ' +
                'concreto. Respecto a nosotros, la información obtenida sólo podrá ser asociada a un usuario concreto en caso ' +
                'de que dicho usuario esté identificado en la web. De ser así, dichos datos serán tratados de conformidad la ' +
                'política de privacidad de la Web, cumpliendo en todo momento con las exigencias de la normativa vigente en ' +
                'materia de protección de datos de carácter personal. En cualquier momento los usuarios afectados podrán ' +
                'ejercitar su derecho a acceder, rectificar y suprimir los datos, así como otros derechos como se explica en ' +
                'la citada política de privacidad.</p><br>',
            manageCookiesTitle: '¿Cómo puedo gestionar las cookies?',
            manageCookiesDescription: 'Los usuarios de la web tienen la opción de no recibir cookies, borrarlas o de ser informados ' +
                'acerca de su fijación mediante la configuración de su navegador. Para saber cómo gestionar las cookies en su navegador, ' +
                'le invitamos a que consulte la ayuda del mismo.<br>' +
                'Para su comodidad, a continuación encontrará información proporcionada por los desarrolladores de los principales ' +
                'navegadores sobre la gestión de cookies:<br>' +
                'Chrome, <a href="http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647" target="_blank">' +
                'http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647</a><br>' +
                'Explorer, <a href="http://windows.microsoft.com/es-es/windows-vista/block-or-allow-cookies" target="_blank">' +
                'http://windows.microsoft.com/es-es/windows-vista/block-or-allow-cookies</a><br>' +
                'Firefox, <a href="http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank">' +
                'http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</a><br>' +
                'Safari, <a href="https://support.apple.com/kb/index?q=manage+cookies&src=globalnav_support&type=' +
                'organic&page=search&locale=es_ES" target="_blank">https://support.apple.com/kb/index?q=manage+cookies&' +
                'src=globalnav_support&type=organic&page=search&locale=es_ES</a><br><br><br>' +
                'En caso de desactivar las cookies, es posible que no pueda hacer uso de todas las funcionalidades de la web.<br>' +
                'Para controlar los pixeles o códigos de seguimiento incluidos en los correos electrónicos, puede configurar su ' +
                'cliente de correo electrónico para que no descargue contenido remoto o imágenes de forma automática.<br>' +
                'Para inhabilitar el uso de cookies publicitarias de terceros, los usuarios pueden igualmente visitar la página de ' +
                'inhabilitación de la Iniciativa publicitaria de la red (NAI, Network Advertising Initiative. Página en inglés: ' +
                '<a href="http://www.networkadvertising.org/managing/opt_out.asp" target="_blank">http://www.networkadvertising.org/' +
                'managing/opt_out.asp</a> ).',
        }
    };

    onScroll(e) {
        if (e.detail.deltaY < 0) {
            $('.move-var').css('top', '0');
        } else if (e.detail.deltaY >= 0) {
            $('.move-var').css('top', -100);
        }
    }

    ngOnInit() {
    }

}
