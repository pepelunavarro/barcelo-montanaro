export class FirebaseProfile {
    id: string;
    name: string;
    lastNames: string;
    email: string;
    avatar: string;
    position: string;
}
