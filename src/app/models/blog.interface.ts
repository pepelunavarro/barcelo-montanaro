export class FirebaseBlog {
    id: string;
    title: string;
    content: string;
    img: string;
}
