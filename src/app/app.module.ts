import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {firebaseConfig} from './credentials';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {IonicStorageModule} from '@ionic/storage';
import {AgmJsMarkerClustererModule} from '@agm/js-marker-clusterer';
import {AgmSnazzyInfoWindowModule} from '@agm/snazzy-info-window';
import {AgmCoreModule} from '@agm/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GoogleAnalytics} from '@ionic-native/google-analytics/ngx';
import {Globalization} from '@ionic-native/globalization/ngx';
import {FireUploaderModule} from '@ngx-fire-uploader/core';
import {FirePhotoModule} from '@ngx-fire-uploader/photo';
import {FireManagerModule} from '@ngx-fire-uploader/manager';
import { FileUploadModule } from 'ng2-file-upload';
import {LanguageMenuPageModule} from './language-menu/language-menu.module';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot({
            name: 'elZagal',
            driverOrder: ['indexeddb', 'sqlite', 'websql']
        }),
        AgmCoreModule.forRoot({
            apiKey: firebaseConfig.mapApiKey
        }),
        AgmSnazzyInfoWindowModule,
        AgmJsMarkerClustererModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFirestoreModule,
        LanguageMenuPageModule,
        ServiceWorkerModule.register('service-worker.js', {enabled: environment.production}),
        FireUploaderModule.forRoot(),
        FirePhotoModule.forRoot(),
        FireManagerModule.forRoot({
            extensions: {
                pdf: 'url("assets/pdf-icon.png")',
                doc: '#335599'
            }
        }),
        FileUploadModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        GoogleAnalytics,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        SocialSharing,
        Globalization
    ],
    bootstrap: [AppComponent],
})

export class AppModule {}
