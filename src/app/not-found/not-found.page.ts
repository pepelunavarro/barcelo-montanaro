import { Component, OnInit } from '@angular/core';
import {FirestoreService} from '../services/data/firestore.service';
import {Router} from '@angular/router';
import {SeoService} from '../services/seo/seo.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.page.html',
  styleUrls: ['./not-found.page.scss'],
})
export class NotFoundPage implements OnInit {

    screenW = window.innerWidth;
    public slider;
    lang;
    commonLanguages;

    constructor(
        public firestore: FirestoreService,
        public router: Router,
        private seo: SeoService
    ) {
        this.lang = localStorage.getItem('lang');
        this.commonLanguages = this.firestore.getData('commonLanguages', this.lang).valueChanges();
    }
  ngOnInit() {
      $('.scroll-down-mouse').fadeOut();
  }

}
