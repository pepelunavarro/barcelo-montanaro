import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { FirebaseBlog } from '../../models/blog.interface';
import { FirebaseProfile } from '../../models/profile.interface';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(public firestore: AngularFirestore) { }

    getBlogList(limit: number, lang: string): AngularFirestoreCollection<FirebaseBlog> {
        return this.firestore.collection(`news`, ref => ref.where('lang', '==', lang).orderBy('timestamp', 'asc').limit(limit));
    }

    getEventsList(lang: string, limit: number) {
      const now = new Date().getTime();
      return this.firestore.collection('events').doc(lang).collection(`allEvents`, ref => ref
          .where('timestamp', '>=', now)
          .orderBy('timestamp', 'asc')
          .limit(limit));
    }

    getBlog(id: string) {
        return this.firestore.doc(`news/${id}`);
    }

    getEvent(lang: string, id: string) {
        return this.firestore.collection(`events`).doc(lang).collection('allEvents', ref => ref.where('id', '==', id));
    }

    getData(collection: string, lang: string) {
        return this.firestore.collection(collection).doc(lang);
    }

    getStores(collection: string, lang: string) {
        return this.firestore.collection(collection).doc(lang).collection('stores');
    }

    setDataArray(collection: string, data: any, id: string) {
        return this.firestore.collection(collection).doc('en').collection('stores').doc(id).set(data);
    }

    setData(collection: string, lang: string, data: any) {
        return this.firestore.collection(collection).doc(lang).set(data);
    }

    updateData(collection: string, lang: string, data: any) {
        return this.firestore.collection(collection).doc(lang).update(data);
    }

    getRecipes(collection: string, lang: string) {
        return this.firestore.collection(collection).doc(lang).collection('allRecipes', ref => ref.orderBy('order', 'desc'));
    }

    getRecipe(collection: string, lang: string, id) {
        return this.firestore.collection(collection).doc(lang).collection('allRecipes').doc(id);
    }

    getProfileList(): AngularFirestoreCollection<FirebaseProfile> {
        return this.firestore.collection(`profile`, ref => ref.orderBy('order'));
    }

    getProjectsList(limit: number): AngularFirestoreCollection<FirebaseProfile> {
        return this.firestore.collection(`projects`, ref => ref.orderBy('order').limit(limit));
    }

    getProject(id: string): AngularFirestoreDocument<FirebaseBlog> {
        return this.firestore.doc(`projects/${id}`);
    }

    getCategoriesList(cat: string): AngularFirestoreCollection<FirebaseProfile> {
        return this.firestore.collection(cat, ref => ref.orderBy('order', 'asc'));
    }

    sendContact(
        name: string,
        email: string,
        lang: string,
        message: string,
        timestamp: any,
    ): Promise<void> {
        const id = this.firestore.createId();
        return this.firestore.doc(`contactMessages/${id}`).set({
            id,
            name,
            email,
            lang,
            message,
            timestamp
        });
    }
    sendSubscribe(
        name: string,
        email: string,
        message: string,
        lang: string,
        timestamp: any,
    ): Promise<void> {
        const id = this.firestore.createId();
        return this.firestore.doc(`subscribesMessages/${id}`).set({
            id,
            name,
            email,
            lang,
            message,
            timestamp
        });
    }
    sendCV(
        name: string,
        email: string,
        lang: string,
        message: string,
        subject: string,
        timestamp: any,
    ): Promise<void> {
        const id = this.firestore.createId();
        return this.firestore.doc(`workWithUsMessages/${id}`).set({
            id,
            name,
            email,
            lang,
            message,
            subject,
            timestamp
        });
    }
}
