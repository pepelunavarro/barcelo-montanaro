import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import {IonicModule} from '@ionic/angular';
import {firebaseConfig} from '../credentials';
import {ContactPage} from './contact.page';
import {ComponentsModule} from '../components/components.module';
import * as Hammer from 'hammerjs';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

const routes: Routes = [
    {
        path: '',
        component: ContactPage
    }
];

export class HammerConfig extends HammerGestureConfig {
    overrides = <any>{
      'swipe': { direction: Hammer.DIRECTION_ALL }
    };
  }

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes),
        ComponentsModule, AgmCoreModule.forRoot({
      apiKey: firebaseConfig.mapApiKey
    })
    ],
    providers: [
      {
        provide: HAMMER_GESTURE_CONFIG,
        useClass: HammerConfig
      }
    ],
    declarations: [ContactPage]
})
export class ContactPageModule {
}
