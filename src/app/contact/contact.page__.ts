import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertController, LoadingController} from '@ionic/angular';
import {FirestoreService} from '../services/data/firestore.service';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import {SeoService} from '../services/seo/seo.service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.page.html',
    styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

    public createContactForm: FormGroup;

    public lang = localStorage.getItem('lang');
    screenW = window.innerWidth;
    commonLanguages;
    contactSection;
    public languages = {
        es: {
            schedulesTitle: 'Horarios',
            beachClub: 'Beach Club 11:00h - 20:30h',
            swimmingPool: 'Piscina 11:00h - 19:00h',
            restaurant: 'Restaurante 13:00h - 16:30h | 19:00h - 22:00h',
            whereTitle: 'Encuéntranos en',
            whereText: '<a href="https://goo.gl/maps/bQuVQ3oa3uJ8huyv8" target="_blank"><p>L’Oronella, s/n. Urb. Puig de Ros</p>' +
                '<p>07609 Llucmajor</p>' +
                '<p>Mallorca. Illes Balears</p></a>',
            warning: 'Atención!',
            okHeader: 'Mensaje enviado',
            okMessage: 'Gracias por contactar con nosotros. ' +
                '<br>Nos pondremos en contacto con usted si el mensaje así lo requiere.',
            contactTitle: 'Teléfonos',
            contactText: '<p>Beach Club <a href="tel+34871038018">+34 871 038 018</a></p>' +
                '<p>Administration <a href="tel+34971213691">+34 971 213 691</a></p>',
            contactForm: {
                policy: 'Antes de enviar el formulario, debe leer la Información básica sobre ' +
                    '<strong>protección de datos</strong>. Al pulsar el botón de envío manifiesta haber leído esta información.',
                offerAgree: 'Deseo recibir ofertas y comunicaciones comerciales ' +
                    'por medios electrónicos. <strong>Política de marketing</strong>',
                urlMarketing: '/politica-de-marketing',
                button: 'Enviar',
                url: '/politica-de-privacidad',
                legalText: 'Información básica sobre privacidad: El responsable del tratamiento es ' +
                    '_SOCIAL_' +
                    'La finalidad es la atención a su solicitud o ' +
                    'consulta y la mejora de la calidad de los servicios ofrecidos. Tiene ' +
                    'derecho a acceder, rectificar y suprimir los datos, así como otros derechos ' +
                    'como se explica en nuestra <strong>política de privacidad Web</strong>'
            },
            policyError: 'Debe aceptar nuestra política de privacidad'
        },
        en: {
            schedulesTitle: 'SCHEDULES',
            beachClub: 'Beach Club 11:00h - 20:30h',
            swimmingPool: 'Swimming Pool 11:00h - 19:00h',
            restaurant: 'Restaurant 13:00h - 16:30h | 19:00h - 22:00h',
            whereTitle: 'Where we are',
            whereText: '<a href="https://goo.gl/maps/bQuVQ3oa3uJ8huyv8" target="_blank"><p>L’Oronella, s/n. Urb. Puig de Ros</p>' +
                '<p>07609 Llucmajor</p>' +
                '<p>Mallorca. Illes Balears</p></a>',
            warning: 'Warning!',
            okHeader: 'Message sent',
            okMessage: 'Thanks for your message',
            contactTitle: 'PHONES',
            contactText: '<p>Beach Club <a href="tel+34871038018">+34 871 038 018</a></p>' +
                '<p>Administration <a href="tel+34971213691">+34 971 213 691</a></p>',
            contactForm: {
                policy: 'Prior to sending the form, you should read the basic <strong>data protection</strong>' +
                    'By clicking on the send button, you state that you have read this information.',
                offerAgree: 'I wish to receive offers and commercial communications' +
                    ' by electronic means. <strong>Marketing policy </strong>',
                urlMarketing: '/marketing-policy',
                button: 'Send',
                url: '/privacy-policy',
                legalText: 'Basic privacy information: The processing controller is _SOCIAL_ ' +
                    'The purpose is the management of our distribution list and the sending of commercial communications. ' +
                    'You have the right of access, to rectify and erase the data, as well as any other rights explained ' +
                    'in our Website <strong>privacy policy</strong>',
            },
            policyError: 'You must accept our privacy policy'
        },
        de: {
            schedulesTitle: 'SCHEDULES',
            beachClub: 'Beach Club 11:00h - 20:30h',
            swimmingPool: 'Swimming Pool 11:00h - 19:00h',
            restaurant: 'Restaurant 13:00h - 16:30h | 19:00h - 22:00h',
            whereTitle: 'Where we are',
            whereText: '<a href="https://goo.gl/maps/bQuVQ3oa3uJ8huyv8" target="_blank"><p>L’Oronella, s/n. Urb. Puig de Ros</p>' +
                '<p>07609 Llucmajor</p>' +
                '<p>Mallorca. Illes Balears</p></a>',
            warning: 'Warning!',
            okHeader: 'Message sent',
            okMessage: 'Thanks for your message',
            contactText: '<p>Beach Club <a href="tel+34871038018">+34 871 038 018</a></p>' +
                '<p>Administration <a href="tel+34971213691">+34 971 213 691</a></p>',
            contactForm: {
                policy: 'Prior to sending the form, you should read the basic <strong>data protection</strong>' +
                    'By clicking on the send button, you state that you have read this information.',
                offerAgree: 'I wish to receive offers and commercial communications' +
                    ' by electronic means. <strong>Marketing policy </strong>',
                urlMarketing: '/marketing-policy',
                button: 'Send',
                url: '/privacy-policy',
                legalText: 'Basic privacy information: The processing controller is _SOCIAL_ ' +
                    'The purpose is the management of our distribution list and the sending of commercial communications. ' +
                    'You have the right of access, to rectify and erase the data, as well as any other rights explained ' +
                    'in our Website <strong>privacy policy</strong>',
            },
            policyError: 'Sie müssen unsere Datenschutzerklärung akzeptieren.'
        }
    };

    constructor(
        public firestore: FirestoreService,
        public router: Router,
        public formBuilder: FormBuilder,
        public loadingCtrl: LoadingController,
        public alert: AlertController,
        public seo: SeoService
    ) {
        this.lang = localStorage.getItem('lang');
        this.commonLanguages = this.firestore.getData('commonLanguages', this.lang).valueChanges();
        this.contactSection = this.firestore.getData('contactSection', this.lang).valueChanges();
        this.createContactForm = formBuilder.group({
            name: ['', Validators.required],
            email: ['', Validators.required],
            subject: ['', Validators.required],
            message: ['', Validators.required],
            policy: [false, null],
            offerAgree: null,
        });
    }

    static sendEmail(array: any, target: string) {
        const params = {
            'sendForm': target,
            'data': array,
            'user': localStorage.user
        };

        return $.ajax({
            data: params,
            type: 'POST',
            dataType: 'jsonp',
            url: 'https://dragut.net/sendEmails/sendMhares.php'
        });
    }

    async sendContactForm() {

        if (this.createContactForm.value.policy === false) {
            this.presentAlert(this.languages[this.lang].warning, this.languages[this.lang].policyError, ['Ok']);
        } else {
            const loading = await this.loadingCtrl.create();
            loading.present();
            const name = this.createContactForm.value.name;
            const email = this.createContactForm.value.email;
            const lang = this.lang ;
            const message = this.createContactForm.value.message;
            const timestamp = new Date();
            const promise = ContactPage.sendEmail(this.createContactForm.getRawValue(), 'mharesContactForm');
            promise.done(function (data) {
                console.log(data);
            });

            promise.fail(function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            });

            this.firestore
                .sendContact(name, email, message, lang, timestamp)
                .then(
                    () => {
                        loading.dismiss().then(() => {
                            this.createContactForm.reset();
                            this.presentAlert(this.languages[this.lang].okHeader, this.languages[this.lang].okMessage, ['Ok']);
                        });
                    },
                    error => {
                        console.error(error);
                    }
                );
            return await loading.present();
        }
    }

    async presentAlert(title, subTitle, buttons) {
        const alert = await this.alert.create({
            header: title,
            subHeader: '',
            message: subTitle,
            buttons: buttons
        });
        await alert.present();
    }

    onScroll(e) {
        if (e.detail.scrollTop > 50) {
            $('.move-var').css('top', '0');
        } else if (e.detail.scrollTop <= 50) {
            $('.move-var').css('top', -100);
        }
    }

    ngOnInit() {
        $('.scroll-down-mouse').fadeOut();
        /*
        this.seo.addTwitterCard(
            '',
            '',
            ''
        );

        this.seo.addFacebookMeta( '',
            '',
            '',
            ''
        );
*/
    }

}
