import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-our-stores',
  templateUrl: './our-stores.page.html',
  styleUrls: ['./our-stores.page.scss'],
})
export class OurStoresPage implements OnInit {

  public vista:String = "m";
  public lang = localStorage.getItem('lang');
  public languages = {
        es: {
            title: 'Nuestras tiendas',
            segment1: 'BM | Mobiliario',
       		segment2: 'BM | Estudio',
       		segment3: 'BM | Maxalto',
       		blockquote_mobiliario: 'En <span>B&M Mobiliario</span> encontrarás un amplio catálogo con la marcas más exclusivas. Fuente de inspiración, descubre nuestras propuestas en mobiliario, accesorios, alfombras, revestimientos y mucho más, seleccionadas meticulosamente.',
       		blockquote_estudio: 'En homenaje a nuestros principios, así nos gusta llamar cariñosamente este espacio donde gestionamos y plani!camos todos nuestros proyectos personalizados de <span>decoración, interiorismo integral y arquitectura</span>.',
       		blockquote_maxalto: 'Estamos orgullosos de haber sido escogidos por <span>Maxalto</span> como su distribuidor en exclusiva, uniendo así nuestro talento con su elaboración artesanal, materiales nobles y excelencia en el diseño. Una mezcla muy atractiva.',
       		address_mobiliario: 'Calle Conflent 1B · 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
       		address_estudio: 'Calle Conflent 1A . 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
       		address_maxalto: 'Calle Conflent 1A . 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
          contacta:'Contacta con' 
        },
        en: {
            title: 'Nuestras tiendas',
            segment1: 'BM | Mobiliario',
       		segment2: 'BM | Estudio',
       		segment3: 'BM | Maxalto',
       		blockquote_mobiliario: 'En <span>B&M Mobiliario</span> encontrarás un amplio catálogo con la marcas más exclusivas. Fuente de inspiración, descubre nuestras propuestas en mobiliario, accesorios, alfombras, revestimientos y mucho más, seleccionadas meticulosamente.',
      		blockquote_estudio: 'En homenaje a nuestros principios, así nos gusta llamar cariñosamente este espacio donde gestionamos y plani!camos todos nuestros proyectos personalizados de <span>decoración, interiorismo integral y arquitectura</span>.',
     		blockquote_maxalto: 'Estamos orgullosos de haber sido escogidos por <span>Maxalto</span> como su distribuidor en exclusiva, uniendo así nuestro talento con su elaboración artesanal, materiales nobles y excelencia en el diseño. Una mezcla muy atractiva.',
         	address_mobiliario: 'Calle Conflent 1B · 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
       		address_estudio: 'Calle Conflent 1A . 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
       		address_maxalto: 'Calle Conflent 1A . 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
          contacta:'Contacta con' 
       },
        de: {
            title: 'Nuestras tiendas',
            segment1: 'BM | Mobiliario',
       		segment2: 'BM | Estudio',
       		segment3: 'BM | Maxalto',
      		blockquote_mobiliario: 'En <span>B&M Mobiliario</span> encontrarás un amplio catálogo con la marcas más exclusivas. Fuente de inspiración, descubre nuestras propuestas en mobiliario, accesorios, alfombras, revestimientos y mucho más, seleccionadas meticulosamente.',
       		blockquote_estudio: 'En homenaje a nuestros principios, así nos gusta llamar cariñosamente este espacio donde gestionamos y plani!camos todos nuestros proyectos personalizados de <span>decoración, interiorismo integral y arquitectura</span>.',
     		blockquote_maxalto: 'Estamos orgullosos de haber sido escogidos por <span>Maxalto</span> como su distribuidor en exclusiva, uniendo así nuestro talento con su elaboración artesanal, materiales nobles y excelencia en el diseño. Una mezcla muy atractiva.',
         	address_mobiliario: 'Calle Conflent 1B · 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
       		address_estudio: 'Calle Conflent 1A . 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
       		address_maxalto: 'Calle Conflent 1A . 07012 Palma de Mallorca<br />Telf. 971 72 63 13',
          contacta:'Contacta con' 
        }
  }

  constructor() { }

  ngOnInit() {
  }

}
