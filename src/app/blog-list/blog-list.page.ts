import {Component, OnInit} from '@angular/core';

import * as $ from 'jquery';
import {FirestoreService} from '../services/data/firestore.service';
import {Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {SeoService} from '../services/seo/seo.service';

@Component({
    selector: 'app-blog-list',
    templateUrl: './blog-list.page.html',
    styleUrls: ['./blog-list.page.scss'],
})
export class BlogListPage implements OnInit {


    screenW = window.innerWidth;
    public slider;
    lang;
    commonLanguages;
    homeSection;
    newsSection;
    gallerySection;
    news;
    events;
    upcomingEvents;
    shadow;
    timestamp;
    date = [];

    constructor(
        public firestore: FirestoreService,
        public router: Router,
        public loadingCtrl: LoadingController,
        private seo: SeoService
    ) {
        const months_arr = {
            'en':
                ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            'es':
                ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        };

        const days_arr = {
            'en':
                ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            'es':
                ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', ' Sab']
        };

        const words = {
            'en': {
                of: ' of '
            },
            'es': {
                of: ' de '
            }
        };

        this.lang = localStorage.getItem('lang');
        this.commonLanguages = this.firestore.getData('commonLanguages', this.lang).valueChanges();
        this.events = this.firestore.getData('eventsSection', this.lang).valueChanges();
        this.newsSection = this.firestore.getData('newsSection', this.lang).valueChanges();
        this.upcomingEvents = this.firestore.getEventsList(this.lang, 4).valueChanges();
        this.news = this.firestore.getBlogList(50, this.lang).valueChanges();
        this.news.subscribe( data => {
            data.forEach( time => {
                this.timestamp = time.timestamp;
                const date = new Date(this.timestamp);
                const year = date.getFullYear();
                const month = months_arr[this.lang][date.getMonth()];
                const day = date.getDate();
                const dayNumber = date.getDay();
                this.date.push( days_arr[this.lang][dayNumber] + '<br> ' + day + '<br>' + month);
            });
        });
        this.upcomingEvents.subscribe( data => {
            data.forEach( time => {
                this.timestamp = time.timestamp;
                const date = new Date(this.timestamp);
                const year = date.getFullYear();
                const month = months_arr[this.lang][date.getMonth()];
                const day = date.getDate();
                const dayNumber = date.getDay();
                this.date.push( days_arr[this.lang][dayNumber] + '<br> ' + day + '<br>' + month);
            });
        });

        this.homeSection = this.firestore.getData('homeSection', this.lang).valueChanges();
        this.homeSection.subscribe( slider => {
            this.slider = slider.slider;
        });
        this.gallerySection = this.firestore.getData('gallerySection', this.lang).valueChanges();
    }

    goTo(url) {
        window.open(url, '_blank');
    }

    addShadow(id) {
        $('#' + id).addClass('shadow');
    }

    delShadow(id) {
        $('#' + id).removeClass('shadow');
    }

    addFavToStorage() {}

    onScroll(e) {
        if (e.detail.scrollTop > 50) {
            $('.move-var').css('top', '0');
            $('.scroll-down-mouse').fadeOut();
        } else if (e.detail.scrollTop <= 50) {
            $('.move-var').css('top', -100);
            $('.scroll-down-mouse').fadeIn();
        }
    }

    ngOnInit() {
        $('.scroll-down-mouse').fadeOut();
/*
        this.seo.addTwitterCard(
            '',
            '',
            ''
        );

        this.seo.addFacebookMeta( '',
            '',
            '',
            ''
        );
*/
    }
}
