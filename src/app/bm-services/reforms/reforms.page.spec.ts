import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReformsPage } from './reforms.page';

describe('ReformsPage', () => {
  let component: ReformsPage;
  let fixture: ComponentFixture<ReformsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReformsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReformsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
