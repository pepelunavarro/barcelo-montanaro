import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reforms',
  templateUrl: './reforms.page.html',
  styleUrls: ['./reforms.page.scss'],
})
export class ReformsPage implements OnInit {

  public lang = localStorage.getItem('lang');
  public languages = {
        es: {
            title: 'Reformas',
            paragraph1: 'A través de nuestro equipo de arquitectura y de una extensa red de profesionales que abarca todos los ámbitos de actuación (electricidad, fontanería...), desarrollamos proyectos de reforma tanto parciales como integrales. Nuestro servicio se destina tanto a particulares como a empresas y contamos con una dilatada experiencia en sectores como la hostelería o incluso el sector náutico, ofreciendo soluciones especiales para embarcaciones.',
       		paragraph2: 'Todas nuestras reformas destacan por la utilización de materiales de primera calidad y están en manos de profesionales cuali!cados y de nuestra máxima confianza.',
       		paragraph3: 'El proceso incluye:<br><br>Análisis de la situación y visita del espacio a reformar Desarrollo de un ante-proyecto<br>Planos detallados 2D-3D<br>Memoria detallada de todos los trabajos y calidades Presupuesto<br>Project Management<br>Reuniones periódicas con el cliente durante la ejecución',
        },
        en: {
            title: 'Reformas',
            paragraph1: 'A través de nuestro equipo de arquitectura y de una extensa red de profesionales que abarca todos los ámbitos de actuación (electricidad, fontanería...), desarrollamos proyectos de reforma tanto parciales como integrales. Nuestro servicio se destina tanto a particulares como a empresas y contamos con una dilatada experiencia en sectores como la hostelería o incluso el sector náutico, ofreciendo soluciones especiales para embarcaciones.',
            paragraph2: 'Todas nuestras reformas destacan por la utilización de materiales de primera calidad y están en manos de profesionales cuali!cados y de nuestra máxima confianza.',
      		paragraph3: 'El proceso incluye:<br><br>Análisis de la situación y visita del espacio a reformar Desarrollo de un ante-proyecto<br>Planos detallados 2D-3D<br>Memoria detallada de todos los trabajos y calidades Presupuesto<br>Project Management<br>Reuniones periódicas con el cliente durante la ejecución',
        },
        de: {
            title: 'Reformas',
            paragraph1: 'A través de nuestro equipo de arquitectura y de una extensa red de profesionales que abarca todos los ámbitos de actuación (electricidad, fontanería...), desarrollamos proyectos de reforma tanto parciales como integrales. Nuestro servicio se destina tanto a particulares como a empresas y contamos con una dilatada experiencia en sectores como la hostelería o incluso el sector náutico, ofreciendo soluciones especiales para embarcaciones.',
            paragraph2: 'Todas nuestras reformas destacan por la utilización de materiales de primera calidad y están en manos de profesionales cuali!cados y de nuestra máxima confianza.',
       		paragraph3: 'El proceso incluye:<br><br>Análisis de la situación y visita del espacio a reformar Desarrollo de un ante-proyecto<br>Planos detallados 2D-3D<br>Memoria detallada de todos los trabajos y calidades Presupuesto<br>Project Management<br>Reuniones periódicas con el cliente durante la ejecución',
        }
  }
  constructor() { }

  ngOnInit() {
  }

}
