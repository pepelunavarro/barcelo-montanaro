import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecorPage } from './decor.page';

describe('DecorPage', () => {
  let component: DecorPage;
  let fixture: ComponentFixture<DecorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
