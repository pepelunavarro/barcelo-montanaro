import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-decor',
  templateUrl: './decor.page.html',
  styleUrls: ['./decor.page.scss'],
})
export class DecorPage implements OnInit {

  public lang = localStorage.getItem('lang');
  public languages = {
        es: {
            title: 'Decoración',
            paragraph1: 'Ofrecemos un completo servicio de decoración tanto para clientes particulares como para empresas, promotores y colectivos.',
       		paragraph2: 'Abarcamos desde proyectos parciales hasta grandes espacios como salones y comedores, pasando por ligeros “liftings” que cambien la !sonomía de un espacio.',
       		paragraph3: 'Nuestro servicio abarca todo lo referente a la decoración del hogar: alfombras, cortinas, papeles pintados, iluminación ropa de cama y mobiliario en general.',
       		paragraph4: 'Cada proyecto es único y se centra en cuatro pasos principales.<br /><br />- Análisis del espacio y exposición del proyecto<br />- Entrega detallada del proyecto (posibilidad en plano)<br />- Ejecución personalizada<br />- Supervisión final',
        },
        en: {
            title: 'Decoración',
            paragraph1: 'Ofrecemos un completo servicio de decoración tanto para clientes particulares como para empresas, promotores y colectivos.',
       		paragraph2: 'Abarcamos desde proyectos parciales hasta grandes espacios como salones y comedores, pasando por ligeros “liftings” que cambien la !sonomía de un espacio.',
       		paragraph3: 'Nuestro servicio abarca todo lo referente a la decoración del hogar: alfombras, cortinas, papeles pintados, iluminación ropa de cama y mobiliario en general.',
       		paragraph4: 'Cada proyecto es único y se centra en cuatro pasos principales.<br /><br />- Análisis del espacio y exposición del proyecto<br />- Entrega detallada del proyecto (posibilidad en plano)<br />- Ejecución personalizada<br />- Supervisión final',
        },
        de: {
            title: 'Decoración',
            paragraph1: 'Ofrecemos un completo servicio de decoración tanto para clientes particulares como para empresas, promotores y colectivos.',
       		paragraph2: 'Abarcamos desde proyectos parciales hasta grandes espacios como salones y comedores, pasando por ligeros “liftings” que cambien la !sonomía de un espacio.',
       		paragraph3: 'Nuestro servicio abarca todo lo referente a la decoración del hogar: alfombras, cortinas, papeles pintados, iluminación ropa de cama y mobiliario en general.',
       		paragraph4: 'Cada proyecto es único y se centra en cuatro pasos principales.<br /><br />- Análisis del espacio y exposición del proyecto<br />- Entrega detallada del proyecto (posibilidad en plano)<br />- Ejecución personalizada<br />- Supervisión final',
        }
  }

  constructor() { }

  ngOnInit() {
  }

}
