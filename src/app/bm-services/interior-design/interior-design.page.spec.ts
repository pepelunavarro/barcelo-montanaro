import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InteriorDesignPage } from './interior-design.page';

describe('InteriorDesignPage', () => {
  let component: InteriorDesignPage;
  let fixture: ComponentFixture<InteriorDesignPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InteriorDesignPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InteriorDesignPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
