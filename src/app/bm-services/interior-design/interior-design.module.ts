import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {ComponentsModule} from '../../components/components.module';
import { IonicModule } from '@ionic/angular';

import { InteriorDesignPage } from './interior-design.page';

const routes: Routes = [
  {
    path: '',
    component: InteriorDesignPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [InteriorDesignPage]
})
export class InteriorDesignPageModule {}
