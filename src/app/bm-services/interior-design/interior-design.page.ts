import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interior-design',
  templateUrl: './interior-design.page.html',
  styleUrls: ['./interior-design.page.scss'],
})
export class InteriorDesignPage implements OnInit {

 public lang = localStorage.getItem('lang');
  public languages = {
        es: {
            title: 'Interiorismo integral',
            paragraph1: 'Nuestro servicio de interiorismo integral se ofrece también para particulares así como para empresas y colectivos. Partiendo de un minucioso análisis de las necesidades de cada cliente y de un completo asesoramiento, planteamos, diseñamos y llevamos a cabo el proyecto integral de decoración y mobiliario de una vivienda al completo, contemplando las particularidades de cada estancia.<br><br>El proyecto incluye todo lo referente a la decoración del hogar: alfombras, cortinas, papeles pintados, iluminación ropa de cama y mobiliario en general.',
       		paragraph2: 'Al igual que en nuestros proyectos de decoración, tratamos cada caso de forma exclusiva en cuatro pasos principales.<br><ul><li>Análisis del espacio y exposición del proyecto</li><li>Entrega detallada del proyecto<br>(posibilidad en plano)</li><li>Ejecución personalizada</li><li>Supervisión final</li></ul>',
        },
        en: {
            title: 'Interiorismo integral',
            paragraph1: 'Nuestro servicio de interiorismo integral se ofrece también para particulares así como para empresas y colectivos. Partiendo de un minucioso análisis de las necesidades de cada cliente y de un completo asesoramiento, planteamos, diseñamos y llevamos a cabo el proyecto integral de decoración y mobiliario de una vivienda al completo, contemplando las particularidades de cada estancia.<br><br>El proyecto incluye todo lo referente a la decoración del hogar: alfombras, cortinas, papeles pintados, iluminación ropa de cama y mobiliario en general.',
       		paragraph2: 'Al igual que en nuestros proyectos de decoración, tratamos cada caso de forma exclusiva en cuatro pasos principales.<br><ul><li>Análisis del espacio y exposición del proyecto</li><li>Entrega detallada del proyecto<br>(posibilidad en plano)</li><li>Ejecución personalizada</li><li>Supervisión final</li></ul>',
        },
        de: {
            title: 'Interiorismo integral',
            paragraph1: 'Nuestro servicio de interiorismo integral se ofrece también para particulares así como para empresas y colectivos. Partiendo de un minucioso análisis de las necesidades de cada cliente y de un completo asesoramiento, planteamos, diseñamos y llevamos a cabo el proyecto integral de decoración y mobiliario de una vivienda al completo, contemplando las particularidades de cada estancia.<br><br>El proyecto incluye todo lo referente a la decoración del hogar: alfombras, cortinas, papeles pintados, iluminación ropa de cama y mobiliario en general.',
       		paragraph2: 'Al igual que en nuestros proyectos de decoración, tratamos cada caso de forma exclusiva en cuatro pasos principales.<br><ul><li>Análisis del espacio y exposición del proyecto</li><li>Entrega detallada del proyecto<br>(posibilidad en plano)</li><li>Ejecución personalizada</li><li>Supervisión final</li></ul>',
        }
  }

  constructor() { }

  ngOnInit() {
  }

}
