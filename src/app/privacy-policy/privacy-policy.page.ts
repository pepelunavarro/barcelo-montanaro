import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.page.html',
  styleUrls: ['./privacy-policy.page.scss'],
})
export class PrivacyPolicyPage implements OnInit {
    public lang = 'es';
    screenW = window.innerWidth;

    constructor() {
    }

    languages = {
        es: {
            section: 'Política de privacidad',
            title: 'Última actualización: marzo 2019 ',
            text: '<p>Esta política de privacidad se aplica a la web ' +
                '<a href="https://www.mharesclub.com/" target="_blank">' +
                'https://www.mharesclub.com/</a> (en adelante la Web).</p>' +
                '<p>Por favor, léala detenidamente. En ella encontrará información ' +
                'importante sobre el tratamiento de sus datos personales y los derechos ' +
                'que le reconoce la normativa vigente en la materia. </p>' +
                '<p>Nos reservamos el derecho de actualizar nuestra política de privacidad ' +
                'en cualquier momento con motivo de decisiones empresariales, así como para ' +
                'cumplir con eventuales cambios legislativos o jurisprudenciales. Si tiene dudas ' +
                'o necesita cualquier aclaración respecto a nuestra Política de privacidad o a sus ' +
                'derechos, puede contactar con nosotros a través de los canales que se indican más abajo.</p>' +
                '<p>Ud. manifiesta que los datos que nos facilite, ahora o en el futuro, son correctos y veraces ' +
                'y se compromete a comunicarnos cualquier modificación de los mismos. En caso de proporcionar ' +
                'datos de carácter personal de terceros, se compromete a obtener el consentimiento previo de los ' +
                'afectados y a informarles acerca del contenido de esta política.</p>' +
                '<p>De manera general, los campos de nuestros formularios que son señalados como obligatorios, ' +
                'deberán necesariamente ser cumplimentados para poder tramitar sus solicitudes.</p>',
            arrayText: [
                {
                    title: '1. ¿Quién es el responsable del tratamiento de sus datos?',
                    text: '<p>Salvo indicación específica en sentido contrario, el responsable del ' +
                        'tratamiento de los datos recogidos en esta Web es _SOCIAL_, ' +
                        'con domicilio en _SOCIAL_ADDRESS, _SOCIAL_CP _SOCIAL_LOCAL, Mallorca, ' +
                        'Illes Balears y correo electrónico<a href="mailto:_SOCIAL_EMAIL">' +
                        '_SOCIAL_EMAIL</a></p>'
                },
                {
                    title: '2. ¿Qué información personal obtenemos?',
                    text: '<p>Los datos que tratamos son los que se obtienen:</p>' +
                        '<ul>' +
                        '<li><p>De los formularios que Ud. cumplimente y de las solicitudes que formule ' +
                        'en la Web. También tratamos los datos incluidos en las solicitudes y peticiones que recibimos por email</p></li>' +
                        '<li><p>De analíticas obtenidos a partir de la navegación de usuarios de la Web, registrados o no.</p></li>' +
                        '</ul>' +
                        '<p>Las categorías de datos que tratamos consisten típicamente en:</p>' +
                        '<ul>' +
                        '<li><p>Datos identificativos y de contacto, DNI o pasaporte</p></li>' +
                        '<li><p>Datos relacionados con su consulta o solicitud</p></li>' +
                        '<li><p>Datos relacionados con su navegación, por ejemplo la dirección IP desde la cual se conecta a ' +
                        'la Web, weblogs, páginas visitadas o acciones realizadas en la Web. Para ello utilizamos cookies ' +
                        'y tecnologías similares que pueden implicar el rastreo de su navegación. Más información en ' +
                        'nuestra política de cookies disponible en <a href="/politica-de-cookies">Política de cookies</a></p></li>' +
                        '</ul>'
                },
                {
                    title: '3. ¿Para qué trataremos sus datos?',
                    text: '<p>Los datos de nuestros usuarios serán tratados para la gestión de la relación con ellos, la ' +
                        'atención a sus solicitudes y consultas, la administración y la gestión de la seguridad de la web ' +
                        'y el cumplimiento de nuestras obligaciones legales. Dichos datos serán igualmente tratados para ' +
                        'fines de analíticas y de mejora de la calidad de nuestros servicios. Asimismo, trataremos sus ' +
                        'datos para el envío de comunicaciones comerciales, si se ha dado de alta en nuestro boletín de ' +
                        'información. </p>'
                },
                {
                    title: '4. ¿A quién podemos comunicar sus datos?',
                    text: '<p>Sus datos sólo serán comunicados a terceros por obligación legal, con su ' +
                        'consentimiento o cuando su petición implique tal comunicación.</p>'
                },
                {
                    title: '5. Base jurídica de los tratamientos',
                    text: '<p>La base para el tratamiento de sus datos es la gestión de la relación jurídica que se ' +
                        'establece con Ud. y el cumplimiento de obligaciones legales.</p>' +
                        '<p>Los siguientes tratamientos de basan en la existencia de los siguientes intereses legítimos:</p>' +
                        '<ul>' +
                        '<li><p>La gestión de la seguridad de la Web se basa en la existencia de nuestro interés ' +
                        'legítimo en garantizar la seguridad de nuestros sistemas.</p></li>' +
                        '<li><p>La realización de estadísticas y control de calidad se basan en nuestro interés ' +
                        'legítimo para evaluar nuestros servicios.</p></li>' +
                        '<li><p>El envío de comunicaciones comerciales se basa en nuestro interés legítimo en ' +
                        'promocionar nuestros servicios, mantenerle informado de novedades legales, así como en ' +
                        'su consentimiento para la recepción de dichas comunicaciones por medios electrónicos.</p></li>' +
                        '</ul>'
                },
                {
                    title: '6. ¿Cuánto tiempo conservaremos sus datos?',
                    text: '<p>De manera general conservamos sus datos durante la vigencia de la relación que mantiene ' +
                        'con nosotros y en todo caso durante los plazos previstos en las disposiciones legales aplicables' +
                        ' y durante el tiempo necesario para atender a posibles responsabilidades nacidas del tratamiento. ' +
                        'Cancelaremos sus datos cuando hayan dejado de ser necesarios o pertinentes para las finalidades para ' +
                        'las cuales fueron recabados. Los logs de acceso a áreas restringidas de la web se cancelarán al mes de ' +
                        'su creación. Se cancelará la información relacionada con la navegación, una vez finalizada la conexión ' +
                        'web y realizadas las estadísticas.</p>' +
                        '<p>Los datos tratados con fines comerciales se conservarán vigentes mientras no se solicite su baja.</p>'
                },
                {
                    title: '7. ¿Cuáles son sus derechos?',
                    text: '<p>Tiene derecho a obtener confirmación de si estamos tratando o no sus datos personales y, en tal caso, ' +
                        'acceder a los mismos. Puede igualmente pedir que sus datos sean rectificados cuando sean inexactos o a que se ' +
                        'completen los datos que sean incompletos, así como solicitar su supresión cuando, entre otros motivos, ' +
                        'los datos ya no sean necesarios para los fines para los que fueron recogidos.</p>' +
                        '<p>En determinadas circunstancias, podrá solicitar la limitación del tratamiento de sus datos. ' +
                        'En tal caso, sólo trataremos los datos afectados para la formulación, el ejercicio o la defensa de ' +
                        'reclamaciones o con miras a la protección de los derechos de otras personas. En determinadas condiciones ' +
                        'y por motivos relacionados con su situación particular, podrá igualmente oponerse al tratamiento de sus ' +
                        'datos. En este caso, dejaremos de tratar los datos salvo por motivos legítimos imperiosos que prevalezcan ' +
                        'sobre sus intereses o derechos y libertades, o para la formulación, el ejercicio o la ' +
                        'defensa de reclamaciones. </p>' +
                        '<p>Así mismo y bajo ciertas condiciones podrá solicitar la portabilidad de sus datos para ' +
                        'que sean transmitidos a otro responsable del tratamiento.</p>' +
                        '<p>Puede revocar el consentimiento que hubiese prestado para determinadas finalidades, sin que ' +
                        'ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada. </p>' +
                        '<p style="text-decoration: underline">Para solicitar su baja de los ' +
                        'tratamientos con fines comerciales puede:</p>' +
                        '<ul>' +
                        '<li><p>Enviar un email a la siguiente dirección de correo electrónico: ' +
                        '<a target="_blank" href="mailto:_SOCIAL_EMAIL">_SOCIAL_EMAIL</a></p></li>' +
                        '<li><p>Utilizar el enlace previsto a tal efecto en nuestros boletines.</p></li>' +
                        '<p>Tiene igualmente el derecho a presentar una reclamación ante una autoridad de protección de datos.</p>' +
                        '<p>Para ejercer sus derechos deberá remitirnos una solicitud acompañada de una copia de ' +
                        'su documento nacional de identidad, u otro documento válido que le identifique por correo ' +
                        'postal o electrónico, a nuestro Delegado de Protección de Datos en las direcciones indicadas ' +
                        'en el apartado ¿Quién es el responsable del tratamiento de sus datos?.	</p>' +
                        '<p>Podrá obtener más información sobre sus derechos y cómo ejercerlos en la página de la Agencia' +
                        ' Española de Protección de Datos en <a target="_blank" href="mailto:chttp://www.aepd.es">' +
                        'http://www.aepd.es</a></p>'
                },
            ]
        }
    };

    onScroll(e) {
        if (e.detail.deltaY < 0) {
            $('.move-var').css('top', '0');
        } else if (e.detail.deltaY >= 0) {
            $('.move-var').css('top', -100);
        }
    }
  ngOnInit() {
  }

}
