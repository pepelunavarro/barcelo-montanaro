import { Component, OnInit } from '@angular/core';
import {FirestoreService} from '../services/data/firestore.service';
import {Router} from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {SeoService} from '../services/seo/seo.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {

    screenW = window.innerWidth;
    public slider;
    lang;
    commonLanguages;
    aboutUsSection;
    contactSection;
    philosophySection;
    workWithUsSection;
    shadow;

    constructor(
        public firestore: FirestoreService,
        public router: Router,
        public loadingCtrl: LoadingController,
        private seo: SeoService
    ) {
        this.lang = localStorage.getItem('lang');
        this.commonLanguages = this.firestore.getData('commonLanguages', this.lang).valueChanges();
        this.aboutUsSection = this.firestore.getData('aboutUsSection', this.lang).valueChanges();
        this.philosophySection = this.firestore.getData('philosophySection', this.lang).valueChanges();
        this.contactSection = this.firestore.getData('contactSection', this.lang).valueChanges();
        this.workWithUsSection = this.firestore.getData('workWithUsSection', this.lang).valueChanges();
        this.aboutUsSection.subscribe( slider => {
            if (this.screenW <= 1180) {
                this.slider = slider.sliderMobile;
            } else {
                this.slider = slider.slider;
            }
        });
    }

    goTo(url) {
        window.open(url, '_blank');
    }

    addShadow() {
        this.shadow = {
            '-webkit-box-shadow': '0 0 10px 0 #a3a3a1',
            '-moz-box-shadow': '0 0 10px 0 #a3a3a1',
            'box-shadow': '0 0 10px 0 #a3a3a1'
        };
    }

    delShadow() {
        this.shadow = {};
    }

    addFavToStorage() {}

    onScroll(e) {
        if (e.detail.scrollTop > 50) {
            $('.move-var').css('top', '0');
            $('.scroll-down-mouse').fadeOut();
        } else if (e.detail.scrollTop <= 50) {
            $('.move-var').css('top', -100);
            $('.scroll-down-mouse').fadeIn();
        }
    }

    ngOnInit() {
/*
        this.seo.addTwitterCard(
            '',
            '',
            ''
        );

        this.seo.addFacebookMeta( '',
            '',
            '',
            ''
        );
*/
    }

}
