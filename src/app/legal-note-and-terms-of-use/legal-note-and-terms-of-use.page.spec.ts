import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalNoteAndTermsOfUsePage } from './legal-note-and-terms-of-use.page';

describe('LegalNoteAndTermsOfUsePage', () => {
  let component: LegalNoteAndTermsOfUsePage;
  let fixture: ComponentFixture<LegalNoteAndTermsOfUsePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalNoteAndTermsOfUsePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalNoteAndTermsOfUsePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
