import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-legal-note-and-terms-of-use',
  templateUrl: './legal-note-and-terms-of-use.page.html',
  styleUrls: ['./legal-note-and-terms-of-use.page.scss'],
})
export class LegalNoteAndTermsOfUsePage implements OnInit {
    public lang = 'es';
    screenW = window.innerWidth;

    constructor() {
    }

    languages = {
        es: {
            title: 'Nota legal y condiciones de uso de la web',
            text: '<p>El acceso a la web https://mharesseaclub.com atribuye la condición ' +
                'de USUARIO e implica la aceptación plena y sin reserva, desde dicho acceso y/o uso, de ' +
                'la presente nota legal y de la política de privacidad en su última versión. Por ello, aconsejamos ' +
                'que proceda a la lectura de estos documentos, antes de utilizar las funcionalidades ofrecidas por ' +
                'este sitio Web, así como cada vez que acceda a él, ya que nos reservamos el derecho de cambiar, ' +
                'modificar, añadir o eliminar en cualquier momento parte de estas condiciones.</p>' +
                '<p>El mero acceso a esta Web no supone en ningún caso la existencia de una relación ' +
                'comercial entre el usuario y la web. </p>',
            arrayText: [
                {
                    title: 'I. TITULAR DE LA WEB',
                    text: '<p>A los efectos de lo previsto por el Art. 10 de la Ley 34/ 2002, de 11 de julio, ' +
                        'de Servicios de la Sociedad de la Información y de Comercio Electrónico, se proporciona ' +
                        'la siguiente información sobre la titular de esta Web:</p>' +
                        '<ul>' +
                        '<li><p>Titular de la Web: COQUIMBAS S.L (Mhares Sea Club, en adelante)</p></li>' +
                        '<li><p>Domicilio social: Avda. Jaime III, 4º 2ª, 07012 Palma de Mallorca, Illes Balears</p></li>' +
                        '<li><p>N.I.F. B-57699498 </p></li>' +
                        '<li><p>Datos registrales: inscrita en el registro Mercantil de Palma de Mallorca, Tomo ___, Libro ___, ' +
                        'Folio ___, Hoja ____, Inscripción __</p></li>' +
                        '</ul>'
                },
                {
                    title: 'II. CONDICIONES DE USO',
                    text: '<p><b>1. Introducción. Mhares Sea Club</b> ofrece en esta web información acerca de ' +
                        'los servicios prestados. Accediendo y utilizando esta web, usted acuerda obligarse a' +
                        ' las condiciones que a continuación se exponen, por lo que se le recomienda que lea cuidadosamente esta' +
                        ' sección antes de iniciar su navegación. </p>' +
                        '<p><b>2. Obligaciones de los Usuarios</b>. Los Usuarios se comprometen a utilizar esta Web de forma ' +
                        'lícita, de acuerdo con lo dispuesto en las presentes condiciones y de forma que no produzcan un ' +
                        'perjuicio en los derechos o intereses de <b>Mhares Sea Club</b> o de terceros. A título enunciativo y en ' +
                        'ningún caso limitativo o excluyente, el Usuario se compromete igualmente a:</p>' +
                        '<ul>' +
                        '<li>' +
                        '<p>No incurrir en actividades ilegales o contrarias al orden público o a la buena fe</p>' +
                        '</li>' +
                        '<li>' +
                        '<p>No utilizar los datos publicados en la Web para el envío de comunicaciones no solicitadas (spam).</p>' +
                        '</li>' +
                        '<li>' +
                        '<p>No introducir o difundir en la web información o contenidos falsos, engañosos, ambiguos o inexactos ' +
                        'de forma que induzcan a error a los receptores de la información, ni tampoco difundir contenidos de ' +
                        'carácter racista, xenófobo, pornográfico, de apología del terrorismo o atentatorio contra los derechos ' +
                        'humanos o de los menores;</p>' +
                        '</li>' +
                        '<li>' +
                        '<p>No realizar acciones que supongan o conlleven una violación de los derechos de propiedad ' +
                        'intelectual <b>Mhares Sea Club</b> o de terceros;</p>' +
                        '</li>' +
                        '<li>' +
                        '<p>No provocar daños en los sistemas físicos y lógicos de <b>Mhares Sea Club</b>, de sus proveedores ' +
                        'o de terceras personas</p>' +
                        '</li>' +
                        '<li>' +
                        '<p>No introducir o difundir en la red virus informáticos o utilizar cualesquiera otros ' +
                        'sistemas físicos o lógicos que sean susceptibles de provocar los daños anteriormente mencionados.</p>' +
                        '</li>' +
                        '<li>' +
                        '<p>No suprimir, alterar, eludir o manipular cualquier dispositivo de protección o sistema de ' +
                        'seguridad que estuviera instalado en las páginas de esta Web</p>' +
                        '</li>' +
                        '</ul>' +
                        '<p><b>3. Exclusión de responsabilidad. Esta Web se proporciona “tal como está” y su uso se realiza bajo ' +
                        'el propio riesgo del Usuario, por lo que, ni Mhares Sea Club, ni sus administradores, trabajadores, ' +
                        'proveedores o colaboradores serán responsables por los daños, de cualquier naturaleza, directos o no, ' +
                        'que deriven del uso de la Web, excluyendo expresamente Mhares Sea Club, en toda la medida prevista por ' +
                        'la Ley, cualquier tipo de garantías, ya sean expresas o implícitas.</b></p>' +
                        '<p><b>Mhares Sea Club</b> no garantiza la disponibilidad y accesibilidad de la Web, si bien ' +
                        'realizará todos los esfuerzos que sean razonables en este sentido. En ocasiones, podrán producirse' +
                        ' interrupciones por el tiempo que resulte necesario para la realización de las correspondientes ' +
                        'operaciones de mantenimiento.</p>' +
                        '<p><b>Mhares Sea Club</b> no se responsabiliza de posibles daños derivados de interferencias, ' +
                        'interrupciones, virus informáticos, averías telefónicas o desconexiones telefónicas motivadas ' +
                        'por causas ajenas a la citada entidad; de retrasos o bloqueos en el uso del presente sistema ' +
                        'electrónico causados por deficiencias o sobrecargas en el centro procesador de datos, de líneas ' +
                        'telefónicas, en el sistema de Internet o en otros sistemas eléctricos; ni tampoco de cualquier otra ' +
                        'alteración que se pueda producir en el Software o Hardware de los Usuarios. Tampoco garantiza la ' +
                        'ausencia de virus, malwares, troyanos u otros elementos que puedan producir alteraciones en el ' +
                        'sistema informático, documentos o ficheros del Usuario, excluyendo cualquier responsabilidad por ' +
                        'los daños de cualquier clase causados al Usuario por este motivo. De igual manera, <b>Mhares Sea Club</b> ' +
                        'no responderá por los daños causados por terceras personas mediante intromisiones ilegítimas fuera de ' +
                        'su control. </p>' +
                        '<p>Tampoco responderá de los daños y perjuicios causados por el uso o mala utilización de los ' +
                        'contenidos de la Web, ni por las consecuencias que pudieran derivarse de los errores, defectos u ' +
                        'omisiones en los contenidos que pudieran aparecer en esta Web proporcionados por los propios Usuarios ' +
                        'u otros terceros. <b>Mhares Sea Club</b> no asume obligación ni responsabilidad alguna respecto de aquellos ' +
                        'servicios que no preste directamente.</p>' +
                        '<p><b>4. Enlaces externos.</b> En ningún caso <b>Mhares Sea Club</b> asumirá responsabilidad ' +
                        'alguna por los contenidos de los enlaces pertenecientes a un sitio web ajeno, ni garantizará la ' +
                        'disponibilidad técnica, calidad, fiabilidad, exactitud, amplitud, veracidad, validez y legalidad ' +
                        'de cualquier material o información contenida en ninguno de dichos hipervínculos u otros sitios de Internet. ' +
                        'Igualmente, la inclusión de estas conexiones externas no implicará ningún tipo de asociación, fusión o ' +
                        'participación con las entidades conectadas. </p>' +
                        '<p><b>5. Denegación de acceso y cancelación de las cuentas. Mhares Sea Club</b> se reserva el derecho de ' +
                        'denegar el acceso a la Web, así como a suspender o cancelar las cuentas a aquellos Usuarios que ' +
                        'incumplan las presentes condiciones; en caso de problemas técnicos o de seguridad imprevistos; de ' +
                        'inactividad de la cuenta durante un tiempo significativo; o en cumplimiento de un requerimiento y/o ' +
                        'de una orden policial, judicial o administrativa.</p>' +
                        '<p>Dicha cancelación o suspensión se realizará a exclusiva discreción de <b>Mhares Sea Club</b> y no dará ' +
                        'lugar a ningún tipo de indemnización.</p>' +
                        '<p><b>6. Tratamiento de datos personales.</b> Los Usuarios consienten el tratamiento de sus datos personales ' +
                        'con el alcance y en los términos que se indican en nuestra política de privacidad, la cual está disponible ' +
                        'en la home page de la Web, quedando asimismo dicha política incorporada a las presentes condiciones ' +
                        'por referencia. En dicha política, los Usuarios encontrarán información sobre la manera de ejercitar ' +
                        'sus derechos de acceso, supresión, limitación y rectificación de su información personal, ' +
                        'así como el de oposición a su tratamiento y portabilidad.</p>' +
                        '<p><b>7. Propiedad intelectual y derechos de autor.</b> Sin perjuicio de los contenidos sobre los ' +
                        'cuales terceros ostentasen derechos intelectuales, los derechos de propiedad intelectual de la Web, ' +
                        'el nombre de dominio, su código fuente, diseño y estructura de navegación y elementos en ella ' +
                        'contenidos (a título enunciativo, imágenes, sonido, audio, vídeo, software, o textos; marcas o ' +
                        'logotipos, combinaciones de colores, estructura y diseño, etc.) son titularidad de <b>Mhares Sea Club</b>' +
                        ', a quien corresponde el ejercicio exclusivo de los derechos de explotación de los mismos en cualquier ' +
                        'forma, y, en especial los derechos de reproducción, distribución, comunicación pública y transformación, ' +
                        'conforme a lo previsto por la vigente Ley de Propiedad Intelectual. Quedan expresamente prohibidas la ' +
                        'reproducción, distribución y la comunicación pública, incluida su modalidad de puesta a disposición, de ' +
                        'la totalidad o parte de los contenidos de esta web en cualquier soporte y por cualquier medio técnico, ' +
                        'sin la autorización de <b>Mhares Sea Club</b> o, en su caso, de los terceros que ostenten los derechos ' +
                        'de propiedad intelectual o de autor sobre los contenidos afectados. No obstante lo anterior, el usuario de ' +
                        'la web podrá visualizar los elementos de esta Web e incluso imprimirlos, copiarlos y almacenarlos en el ' +
                        'disco duro de su ordenador o en cualquier otro soporte físico siempre y cuando sea, única y exclusivamente, ' +
                        'para su uso personal y privado. Aquellas entidades o personas que, previa autorización del titular de ' +
                        'esta web, pretendan establecer un enlace con ella, deberán garantizar que únicamente permite el acceso ' +
                        'a esta Web o servicio pero que no realiza reproducción de sus contenidos y servicios.</p>' +
                        '<p><b>8. Nulidad de las cláusulas.</b> En caso de que una cláusula de las presentes condiciones ' +
                        'de uso fuese declarada nula, sólo afectará a dicha disposición o a aquella parte que así haya sido ' +
                        'declarada, subsistiendo las condiciones en todo lo demás y teniéndose tal disposición, o la parte ' +
                        'afectada, por no puesta.</p>' +
                        '<p><b>9. Aceptación.</b> El acceso a la Web y su utilización implican necesariamente que todas y ' +
                        'cada una de las presentes condiciones de uso son expresamente aceptadas por usted. </p>' +
                        '<p><b>10. Ley y jurisdicción aplicables.</b> La presente web se regirá por la ley española con ' +
                        'exclusión de sus reglas de conflicto de ley y cualquier controversia que pudiera derivarse de su ' +
                        'utilización o de los servicios vinculados a la misma se someterá a la jurisdicción y competencia de ' +
                        'los Juzgados y Tribunales de Palma de Mallorca, renunciando expresamente los usuarios a su fuero ' +
                        'propio si lo tuvieran.</p>' +
                        '<p>La Comisión Europea facilita una plataforma de resolución de litigios en línea dirigida ' +
                        'a los consumidores, pudiendo estos acceder a la misma mediante el siguiente enlace: ' +
                        '<a href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.chooseLanguage" ' +
                        'target="_blank">https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.chooseLanguage</a></p>' +
                        '<br>' +
                        '<br>' +
                        '<br>' +
                        '<p>Versión 1.0 – marzo de 2019 </p>'
                },
            ]
        }
    };

    onScroll(e) {
        if (e.detail.deltaY < 0) {
            $('.move-var').css('top', '0');
        } else if (e.detail.deltaY >= 0) {
            $('.move-var').css('top', -100);
        }
    }
  ngOnInit() {
  }

}
