import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageMenuPage } from './language-menu.page';

describe('LanguageMenuPage', () => {
  let component: LanguageMenuPage;
  let fixture: ComponentFixture<LanguageMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanguageMenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
