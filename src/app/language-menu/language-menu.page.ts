import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-language-menu',
  templateUrl: './language-menu.page.html',
  styleUrls: ['./language-menu.page.scss'],
})
export class LanguageMenuPage implements OnInit {

    public lang = localStorage.getItem('lang');

    constructor(public route: Router) {}

    langSwitch(lang) {
        localStorage.setItem('lang', lang);
        this.lang = lang;
        console.log(this.route.url);
        window.location.href = '/' + lang;
    }

    ngOnInit() {}

}
