import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpositionPage } from './exposition.page';

describe('ExpositionPage', () => {
  let component: ExpositionPage;
  let fixture: ComponentFixture<ExpositionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpositionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpositionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
