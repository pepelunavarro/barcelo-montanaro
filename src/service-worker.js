let cache_name = 'el-mhares_v1';

let urls_to_cache = [
        '/',
        '/!**',
        './build/main.js',
        './build/vendor.js',
        './build/main.css',
        './build/polyfills.js',
        'index.html',

        'manifest.json',
        '/assets/!**'

];
self.addEventListener('install', (e) => {
/*
    e.waitUntil(caches.open(cache_name).then((cache) => {
        return cache.addAll(urls_to_cache)
    }) );
*/
    caches.keys().then(function(names) {
        for (let name of names){
            caches.delete(name);
        }
    });

});

self.addEventListener('fetch', function(e) {

    e.respondWith(caches.match(e.request).then((response) => {
        if(response)
            return response;
        else
            return fetch(e.request)
    }))
});


self.addEventListener('beforeinstallprompt', function(e) {
    // beforeinstallprompt Event fired

    // e.userChoice will return a Promise.
    // For more details read: https://developers.google.com/web/fundamentals/getting-started/primers/promises
    e.userChoice.then(function(choiceResult) {

        console.log(choiceResult.outcome);

        if(choiceResult.outcome === 'dismissed') {
            console.log('User cancelled home screen install');
        }
        else {
            console.log('User added to home screen');
        }
    });
});
